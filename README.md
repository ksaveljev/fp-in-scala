# Functional Programming Course

### Introduction

The course is structured according to a linear progression and uses *Scala* programming
language to learn programming concepts pertaining to functional programming. This course
is a direct port from Haskell programming language and might be lacking some beauty and
expressiveness.

All existing code compiles, however answers have been replaced with a call to Scala `???`
function and so the code will throw an exception if it is run. Some exercises contain tips,
which are annotated with a preceding "Tip:". It is not necessary to adhere to tips. Tips
are provided for potential guidance, which may be discarded if you prefer a different
path to a solution.

These exercises are designed in a way that requires personal guidance, so if you attempt
it on your own and feel a little lost, this is normal. All the instructions are not
contained herein.

### Getting started

1. Install *sbt* (build tool for Scala) version 1.0 or higher
   * [Installation instructions for Mac](http://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Mac.html)
   * [Installation instructions for Windows](http://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Windows.html)
   * [Installation instructions for Linux](http://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html)
2. Change to the directory containing this document
3. Execute the command `sbt`, which will compile and load all the source code
4. Inspect the introductory modules to get a feel for Scala's syntax, then move on
to the exercises starting with `Optional`. The [Progression](#progression) section
of this document lists the recommended order in which to attempt the exercises
5. Edit a source file to a proposed solution to an exercise. At the `sbt` prompt,
issue the command `compile`. This will compile your solution and reload it in the
Scala interpreter.

### Tips after getting started

1. Testing your solutions.
   There are a lot of tests written for the given exercises which will help you identify
   whether your solution is correct or not. At the `sbt` prompt, issue the command
   `test`. This will run all the tests for all the exercises. As you solve more and more
   exercises the amount of failed tests should decrease. In order to run tests for a
   specific module, issue the command `testOnly *<module>*` where `module` is the name
   of the module you are working on (e.g. `testOnly *Functor*`, `testOnly *ListZipper*`)
2. Use IDE for some extra bit of help.
   You can use [IntelliJ IDEA](https://www.jetbrains.com/idea/) (community edition) to
   get a somewhat good environment which will help in your journey. Make sure to install
   it with Scala plugin enabled and import this project as SBT project. It can help you
   by showing the types under the cursor, compile the code and run sbt tests. Be warned
   though that IDE will sometimes highlight some code as incorrect although it compiles
   and runs just fine.
3. Follow the types.
   You may find yourself in a position of being unsure how to proceed for a given
   exercise. You are encouraged to adopt a different perspective. Instead of asking how
   to proceed, ask how you might proceed while adhering to the guideline provided by the
   types for the exercise at hand.
   
   It is possible to follow the types without achieving the desired goal, however, this
   is reasonably unlikely at the start. As you become more reliant on following the
   types, you will develop more trust in the potential paths that they can take you,
   including identification of false paths.
   
   Your instructor must guide you where types fall short, but you should also take the
   first step. Do it.

### Progression

It is recommended to perform some exercises before others. The first step is to
inspect the introduction modules.

* `ExactlyOne`
* `Validation`

They contain examples of data structures and Scala syntax. They do not contain
exercises and exist to provide a cursory examination of Scala syntax. The next
step is to complete the exercises in `Optional`.

After this, the following progression of modules is recommended (this list will
be updated when additional exercises are ported from Haskell):

* `List`
* `Functor`
* `Applicative`
* `Monad`
* `State`
* `StateT`
* `Extend`
* `Comonad`
* `Compose`
* `Traversable`
* `ListZipper`

During this progression, it is often the case that some exercises are abandoned
due to time constraints and the benefit of completing some exercises over
others. For example, in the progression, `Functor` to `Monad`, the
exercises repeat a similar theme. Instead, a participant may wish to do
different exercises, such as `Parser`. In this case, the remaining
answers are filled out, so that progress on to `Parser` can begin
(which depends on correct answers up to `Monad`). It is recommended to
take this deviation if it is felt that there is more reward in doing so.

Answers for the exercises can be found in a branch `solutions`
