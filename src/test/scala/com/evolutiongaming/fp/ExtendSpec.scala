package com.evolutiongaming.fp

import org.scalatest.FreeSpec

class ExtendSpec extends FreeSpec {

  import com.evolutiongaming.fp.Extend._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  "exactlyOne Test" -{
    "ExactlyOne instance" in {
      val F: Extend[ExactlyOne] = implicitly
      assert(F.extend(identity[ExactlyOne[Int]])(ExactlyOne(7)) === ExactlyOne(ExactlyOne(7)))
    }
  }

  "List" -{
    val F: Extend[List] = implicitly

    "length" in {
      assert(F.extend(length)('a' :: 'b' :: 'c' :: Nil) === (3 :: 2 :: 1 :: Nil))
    }
    "id" in {
      assert(F.extend(identity[List[Int]])(1 :: 2 :: 3 :: 4 :: Nil) === (1 :: 2 :: 3 :: 4 :: Nil) :: (2 :: 3 :: 4 :: Nil) :: (3 :: 4 :: Nil) :: (4 :: Nil) :: Nil)
    }
    "reverse" in {
      assert(F.extend(reverse[List[Int]])((1 :: 2 :: 3 :: Nil) :: (4 :: 5 :: 6 :: Nil) :: Nil) === ((4 :: 5 :: 6 :: Nil) :: (1 :: 2 :: 3 :: Nil) :: Nil) :: ((4 :: 5 :: 6 :: Nil) :: Nil) :: Nil)
    }
  }

  "Optional" -{
    val F: Extend[Optional] = implicitly

    "id Full" in {
      assert(F.extend(identity[Optional[Int]])(Full(7)) === Full(Full(7)))
    }
    "id Empty" in {
      assert(F.extend(identity[Optional[Int]])(Empty) === (Empty: Optional[Optional[Int]]))
    }
  }

  "cojoin" -{
    "ExactlyOne" in {
      assert(cojoin(ExactlyOne(7)) === ExactlyOne(ExactlyOne(7)))
    }
    "List" in {
      assert(cojoin(1 :: 2 :: 3 :: 4 :: Nil) === (1 :: 2 :: 3 :: 4 :: Nil) :: (2 :: 3 :: 4 :: Nil) :: (3 :: 4 :: Nil) :: (4 :: Nil) :: Nil)
    }
    "Full" in {
      assert(cojoin(Full(7): Optional[Int]) === Full(Full(7)))
    }
    "Empty" in {
      assert(cojoin(Empty) === (Empty: Optional[Optional[Integer]]))
    }
  }

}
