package com.evolutiongaming.fp

import org.scalatest.FreeSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class FunctorSpec extends FreeSpec with GeneratorDrivenPropertyChecks {
  import com.evolutiongaming.fp.Functor._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  "ExactlyOne" -{
    "id test" in {
      assert(exactlyOneFunctor.fmap((x: Int) => x + 1)(ExactlyOne(2)) === ExactlyOne(3))
    }
  }

  "List" -{
    "empty list" in {
      assert(listFunctor.fmap((x: Int) => x + 1)(Nil) === Nil)
    }
    "increment" in {
      assert(listFunctor.fmap((x: Int) => x + 1)(1 :: 2 :: 3 :: Nil) === 2 :: 3 :: 4 :: Nil)
    }
  }

  "Optional" -{
    "Empty" in {
      assert(optionalFunctor.fmap((x: Int) => x + 1)(Empty) === Empty)
    }
    "Full" in {
      assert(optionalFunctor.fmap((x: Int) => x + 1)(Full(2)) === Full(3))
    }
  }

  "Function" -{
    "(=>)" in{
      assert(functionFunctor.fmap((x: Int) => x + 1)((x: Int) => x * 2)(8) === 17)
    }
  }

  "amap" -{
    "Map 7" in {
      assert(listFunctor.amap(7, 1 :: 2 :: 3 :: Nil) === 7 :: 7 :: 7 :: Nil)
    }
    "Always maps a constant value over List" in {
      forAll { (input: (Int, Int, Int, Int)) =>
        val (x, a, b, c) = input
        assert(listFunctor.amap(x, a :: b :: c :: Nil) === x :: x :: x :: Nil)
      }
    }
    "Always maps a constant value over Full (Optional)" in {
      forAll { (input: (Int, Int)) =>
        val (x, y) = input
        assert(optionalFunctor.amap(x, Full(y)) === Full(x))
      }
    }
  }

  "void" -{
    "List" in {
      assert(listFunctor.void(1 :: 2 :: 3 :: Nil) === () :: () :: () :: Nil)
    }
    "Full" in {
      assert(optionalFunctor.void(Full(7)) === Full(()))
    }
    "Empty" in {
      assert(optionalFunctor.void(Empty) === Empty)
    }
    "(=>)" in {
      assert(Full(functionFunctor.void((x: Int) => x + 10)(5)) === Full(()))
    }
  }

}
