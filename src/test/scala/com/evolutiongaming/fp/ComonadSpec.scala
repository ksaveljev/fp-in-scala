package com.evolutiongaming.fp

import org.scalatest.FreeSpec

class ComonadSpec extends FreeSpec {

  import com.evolutiongaming.fp.Comonad._

  "Comonad" -{
    val F: Comonad[ExactlyOne] = implicitly

    "ExactlyOne" in {
      assert(F.copure(ExactlyOne(7)) === 7)
    }

    "fmap" in {
      assert(fmap(ExactlyOne(7))(_ + 10) === ExactlyOne(17))
    }
  }

}
