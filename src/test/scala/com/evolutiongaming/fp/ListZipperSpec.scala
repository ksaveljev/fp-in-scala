package com.evolutiongaming.fp

import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalactic.Equality
import org.scalatest.FreeSpec
import org.scalatest.prop._

import scala.collection.immutable.{List => ScalaList}

class ListZipperSpec extends FreeSpec with GeneratorDrivenPropertyChecks {

  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.ListZipper._
  import com.evolutiongaming.fp.Optional._

  "listZipper functorTest" -{
    "fmap" in {
      val F: Functor[ListZipper] = implicitly
      val expected = ListZipper(4 :: 3 :: 2 :: Nil, 5, 6 :: 7 :: 8 :: Nil)
      assert(F.fmap((x: Int) => x + 1)(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === expected)
    }
  }

  "maybeListZipper functorTest" -{
    "fmap" in {
      val F: Functor[MaybeListZipper] = implicitly
      val expected = IsZ(ListZipper(4 :: 3 :: 2 :: Nil, 5, 6 :: 7 :: 8 :: Nil))
      assert(F.fmap((x: Int) => x + 1)(IsZ(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil))) === expected)
    }
  }

  "fromList" -{
    "non empty list" in {
      assert(fromList(1 :: 2 :: 3 :: Nil) === IsZ(ListZipper(Nil, 1, 2 :: 3 :: Nil)))
    }
    "empty list" in {
      assert(fromList(Nil) === IsNotZ)
    }
  }

  "toOptional" -{
    "toOptional fromList" in {
      forAll(genIntList) { list =>
        assert(list.isEmpty === (toOptional(fromList(list)) === Empty))
      }
    }
    "toOptional fromOptional" in {
      forAll(genIntList) { list =>
        val z: Optional[ListZipper[Int]] = fromList(list) match {
          case IsNotZ  => Empty
          case IsZ(zz) => Full(zz)
        }
        assert(toOptional(fromOptional(z)) === z)
      }
    }
  }

  "toList" -{
    "empty list" in {
      val F: Functor[Optional] = implicitly
      assert(F.fmap(toList)(toOptional(fromList(Nil))) === Empty)
    }
    "non empty list" in {
      assert(toList(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === 1 :: 2 :: 3 :: 4 :: Nil)
    }
    "non empty list 2" in {
      assert(toList(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: Nil)
    }
  }

  "withFocus" -{
    "empty left" in {
      assert(withFocus(ListZipper(Nil, 0, 1 :: Nil))(_ + 1) === ListZipper(Nil, 1, 1 :: Nil))
    }
    "full zipper" in {
      assert(withFocus(ListZipper(1 :: 0 :: Nil, 2, 3 :: 4 :: Nil))(_ + 1) === ListZipper(1 :: 0 :: Nil, 3, 3 :: 4 :: Nil))
    }
  }

  "setFocus" -{
    "empty left" in {
      assert(setFocus(ListZipper(Nil, 0, 1 :: Nil), 1) === ListZipper(Nil, 1, 1 :: Nil))
    }
    "full zipper" in {
      assert(setFocus(ListZipper(1 :: 0 :: Nil, 2, 3 :: 4 :: Nil), 1) === ListZipper(1 :: 0 :: Nil, 1, 3 :: 4 :: Nil))
    }
  }

  "hasLeft" -{
    "non empty left" in {
      assert(hasLeft(ListZipper(1 :: 0 :: Nil, 2, 3 :: 4 :: Nil)) === true)
    }
    "empty left" in {
      assert(hasLeft(ListZipper(Nil, 0, 1 :: 2 :: Nil)) === false)
    }
  }

  "hasRight" -{
    "non empty right" in {
      assert(hasRight(ListZipper(1 :: 0 :: Nil, 2, 3 :: 4 :: Nil)) === true)
    }
    "empty right" in {
      assert(hasRight(ListZipper(1 :: 0 :: Nil, 2, Nil)) === false)
    }
  }

  "findLeft" -{
    "predicate always false" in {
      forAll(genIntList) { list =>
        assert(asMaybeZipper(fromList(list))(x => findLeft(x)(_ => false)) === IsNotZ)
      }
    }
    "find existing" in {
      assert(findLeft(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: Nil))(_ == 1) === IsZ(ListZipper(Nil, 1, 2 :: 3 :: 4 :: 5 :: Nil)))
    }
    "find non existing" in {
      assert(findLeft(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: Nil))(_ == 6) === IsNotZ)
    }
    "find existing 2" in {
      assert(findLeft(ListZipper(2 :: 1 :: Nil, 1, 4 :: 5 :: Nil))(_ == 1) === IsZ(ListZipper(Nil, 1, 2 :: 1 :: 4 :: 5 :: Nil)))
    }
    "find existing 3" in {
      assert(findLeft(ListZipper(1 :: 2 :: 1 :: Nil, 3, 4 :: 5 :: Nil))(_ == 1) === IsZ(ListZipper(2 :: 1 :: Nil, 1, 3 :: 4 :: 5 :: Nil)))
    }
  }

  "findRight" -{
    "predicate always false" in {
      forAll(genIntList) { list =>
        assert(asMaybeZipper(fromList(list))(x => findRight(x)(_ => false)) === IsNotZ)
      }
    }
    "find existing" in {
      assert(findRight(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: Nil))(_ == 5) === IsZ(ListZipper(4 :: 3 :: 2 :: 1 :: Nil, 5, Nil)))
    }
    "find non existing" in {
      assert(findRight(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: Nil))(_ == 6) === IsNotZ)
    }
    "find existing 2" in {
      assert(findRight(ListZipper(2 :: 3 :: Nil, 1, 4 :: 5 :: 1 :: Nil))(_ == 1) === IsZ(ListZipper(5 :: 4 :: 1 :: 2 :: 3 :: Nil, 1, Nil)))
    }
    "find existing 3" in {
      assert(findRight(ListZipper(2 :: 3 :: Nil, 1, 1 :: 4 :: 5 :: 1 :: Nil))(_ == 1) === IsZ(ListZipper(1 :: 2 :: 3 :: Nil, 1, 4 :: 5 :: 1 :: Nil)))
    }
  }

  "moveLeftLoop" -{
    "left non empty" in {
      assert(moveLeftLoop(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: 6 :: 7 :: Nil))
    }
    "left empty" in {
      assert(moveLeftLoop(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil))
    }
  }

  "moveRightLoop" -{
    "right non empty" in {
      assert(moveRightLoop(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(4 :: 3 :: 2 :: 1 :: Nil, 5, 6 :: 7 :: Nil))
    }
    "right empty" in {
      assert(moveRightLoop(ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil))
    }
  }

  "moveLeft" -{
    "left non empty" in {
      assert(moveLeft(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: 6 :: 7 :: Nil)))
    }
    "left empty" in {
      assert(moveLeft(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === IsNotZ)
    }
  }

  "moveRight" -{
    "right non empty" in {
      assert(moveRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(4 :: 3 :: 2 :: 1 :: Nil, 5, 6 :: 7 :: Nil)))
    }
    "right empty" in {
      assert(moveRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === IsNotZ)
    }
  }

  "swapLeft" -{
    "left non empty" in {
      assert(swapLeft(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(4 :: 2 :: 1 :: Nil, 3, 5 :: 6 :: 7 :: Nil)))
    }
    "left empty" in {
      assert(swapLeft(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === IsNotZ)
    }
  }

  "swapRight" -{
    "right non empty" in {
      assert(swapRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(3 :: 2 :: 1 :: Nil, 5, 4 :: 6 :: 7 :: Nil)))
    }
    "right empty" in {
      assert(swapRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === IsNotZ)
    }
  }

  "dropLefts" -{
    "left non empty" in {
      assert(dropLefts(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(Nil, 4, 5 :: 6 :: 7 :: Nil))
    }
    "left empty" in {
      assert(dropLefts(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil))
    }
  }

  "dropRights" -{
    "right non empty" in {
      assert(dropRights(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil))
    }
    "right empty" in {
      assert(dropRights(ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil))
    }
  }

  "moveLeftN" -{
    "positive n" in {
      assert(moveLeftN(2, ListZipper(2 :: 1 :: 0 :: Nil, 3, 4 :: 5 :: 6 :: Nil)) === IsZ(ListZipper(0 :: Nil, 1, 2 :: 3 :: 4 :: 5 :: 6 :: Nil)))
    }
    "negative n" in {
      assert(moveLeftN(-1, ListZipper(2 :: 1 :: 0 :: Nil, 3, 4 :: 5 :: 6 :: Nil)) === IsZ(ListZipper(3 :: 2 :: 1 :: 0 :: Nil, 4, 5 :: 6 :: Nil)))
    }
  }

  "moveRightN" -{
    "positive n" in {
      assert(moveRightN(1, ListZipper(2 :: 1 :: 0 :: Nil, 3, 4 :: 5 :: 6 :: Nil)) === IsZ(ListZipper(3 :: 2 :: 1 :: 0 :: Nil, 4, 5 :: 6 :: Nil)))
    }
    "negative n" in {
      assert(moveRightN(-1, ListZipper(2 :: 1 :: 0 :: Nil, 3, 4 :: 5 :: 6 :: Nil)) === IsZ(ListZipper(1 :: 0 :: Nil, 2, 3 :: 4 :: 5 :: 6 :: Nil)))
    }
  }

  "moveLeftNPrim" -{
    "positive n (left not enough)" in {
      assert(moveLeftNPrim(4, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Left(3))
    }
    "positive n" in {
      assert(moveLeftNPrim(1, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(2 :: 1 :: Nil, 3, 4 :: 5 :: 6 :: 7 :: Nil)))
    }
    "n is 0" in {
      assert(moveLeftNPrim(0, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)))
    }
    "negative n" in {
      assert(moveLeftNPrim(-2, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(5 :: 4 :: 3 :: 2 :: 1 :: Nil, 6, 7 :: Nil)))
    }
    "negative n (right not enough)" in {
      assert(moveLeftNPrim(-4, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Left(3))
    }
    "positive n (left not enough) 2" in {
      assert(moveLeftNPrim(4, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: 8 :: 9 :: Nil)) === Left(3))
    }
    "negative n (right not enough) 2" in {
      assert(moveLeftNPrim(-4, ListZipper(5 :: 4 :: 3 :: 2 :: 1 :: Nil, 6, 7 :: 8 :: 9 :: Nil)) === Left(3))
    }
  }

  "moveRightNPrim" -{
    "positive n (right not enough)" in {
      assert(moveRightNPrim(4, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Left(3))
    }
    "positiven n" in {
      assert(moveRightNPrim(1, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(4 :: 3 :: 2 :: 1 :: Nil, 5, 6 :: 7 :: Nil)))
    }
    "n is 0" in {
      assert(moveRightNPrim(0, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)))
    }
    "negative n" in {
      assert(moveRightNPrim(-2, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Right(ListZipper(1 :: Nil, 2, 3 :: 4 :: 5 :: 6 :: 7 :: Nil)))
    }
    "negative n (left not enough)" in {
      assert(moveRightNPrim(-4, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === Left(3))
    }
  }

  "nth" -{
    "going left" in {
      assert(nth(1, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(1 :: Nil, 2, 3 :: 4 :: 5 :: 6 :: 7 :: Nil)))
    }
    "going right" in {
      assert(nth(5, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(5 :: 4 :: 3 :: 2 :: 1 :: Nil, 6, 7 :: Nil)))
    }
    "going too far" in {
      assert(nth(8, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsNotZ)
    }
  }

  "index" -{
    "current index" in {
      assert(index(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === 3)
    }
    // TODO: prop> optional True (\z' -> index z' == i) (toOptional (nth i z))
  }

  "end" -{
    "move to end" in {
      assert(end(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(6 :: 5 :: 4 :: 3 :: 2 :: 1 :: Nil, 7, Nil))
    }
    "end leaves same elements" in {
      forAll(genIntListZipper) { z =>
        assert(toList(z) === toList(end(z)))
      }
    }
    "end leaves right empty" in {
      forAll(genIntListZipper) { z =>
        assert(rights(end(z)) === Nil)
      }
    }
  }

  "start" -{
    "move to start" in {
      assert(start(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(Nil, 1, 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: Nil))
    }
    "start leaves same elements" in {
      forAll(genIntListZipper) { z =>
        assert(toList(z) === toList(start(z)))
      }
    }
    "start leaves left empty" in {
      forAll(genIntListZipper) { z =>
        assert(lefts(start(z)) === Nil)
      }
    }
  }

  "deletePullLeft" -{
    "left non empty" in {
      assert(deletePullLeft(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(2 :: 1 :: Nil, 3, 5 :: 6 :: 7 :: Nil)))
    }
    "left empty" in {
      assert(deletePullLeft(ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === IsNotZ)
    }
  }

  "deletePullRight" -{
    "right non empty" in {
      assert(deletePullRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === IsZ(ListZipper(3 :: 2 :: 1 :: Nil, 5, 6 :: 7 :: Nil)))
    }
    "right empty" in {
      assert(deletePullRight(ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === IsNotZ)
    }
  }

  "insertPushLeft" -{
    "left non empty" in {
      assert(insertPushLeft(15, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(4 :: 3 :: 2 :: 1 :: Nil, 15, 5 :: 6 :: 7 :: Nil))
    }
    "left empty" in {
      assert(insertPushLeft(15, ListZipper(Nil, 1, 2 :: 3 :: 4 :: Nil)) === ListZipper(1 :: Nil, 15, 2 :: 3 :: 4 :: Nil))
    }
    // TODO:  prop> optional False (==z) (toOptional (deletePullLeft (insertPushLeft i z)))
  }

  "insertPushRight" -{
    "right non empty" in {
      assert(insertPushRight(15, ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)) === ListZipper(3 :: 2 :: 1 :: Nil, 15, 4 :: 5 :: 6 :: 7 :: Nil))
    }
    "right empty" in {
      assert(insertPushRight(15, ListZipper(3 :: 2 :: 1 :: Nil, 4, Nil)) === ListZipper(3 :: 2 :: 1 :: Nil, 15, 4 :: Nil))
    }
    // TODO: prop> optional False (==z) (toOptional (deletePullRight (insertPushRight i z)))
  }

  "listZipper applicative" -{
    /* TODO: stack overflow ?
    "pure lefts" in {
      val F: Applicative[ListZipper] = implicitly

      forAll { (input: (Int, Int)) =>
        val (a, n) = input
        assert(all((x: Int) => x == a)(take(n, lefts(F.pure(a)))))
      }
    }
    "pure rights" in {

      val F: Applicative[List] = implicitly
      val G: Applicative[ListZipper] = implicitly

      forAll { (input: (Int, Int)) =>
        val (a, n) = input
        assert(all((x: Int) => x == a)(take(n, rights(G.pure(a)))))
      }
    }
    */
    "<*>" in {
      val F: Applicative[ListZipper] = implicitly
      val z = ListZipper(3 :: 2 :: 1 :: Nil, 4, 5 :: 6 :: 7 :: Nil)
      val fz = ListZipper(((x: Int) => x + 2) :: ((x: Int) => x + 10) :: Nil, (x: Int) => x * 2, ((x: Int) => x * 3) :: ((x: Int) => x * 4) :: ((x: Int) => x + 5) :: Nil)
      assert(F.apply(fz)(z) === ListZipper(5 :: 12 :: Nil, 8, 15 :: 24 :: 12 :: Nil))
    }
  }









  def genList[A: Arbitrary]: Gen[List[A]] =
    arbitrary[ScalaList[A]].map((v: ScalaList[A]) => v.foldRight(Nil: List[A])((a, b) => a :: b))

  def genIntList: Gen[List[Int]] = genList[Int]

  def genIntListZipper: Gen[ListZipper[Int]] =
    arbitrary[(Int, ScalaList[Int], ScalaList[Int])].map(v => ListZipper(listScala(v._2), v._1, listScala(v._3)))

  import org.scalactic.TripleEquals._

  implicit def eitherIntListZipperEq[A: Equality]: Equality[Either[Int, ListZipper[A]]] = new Equality[Either[Int, ListZipper[A]]] {

    def areEqual(a: Either[Int, ListZipper[A]], other: Any): Boolean = other match {
      case b: Either[_, _] =>
        (a, b) match {
          case (Left(aa), Left(bb))   => aa === bb
          case (Right(aa), Right(bb)) => aa === bb
          case _                      => false
        }

      case _ => false
    }
  }
}
