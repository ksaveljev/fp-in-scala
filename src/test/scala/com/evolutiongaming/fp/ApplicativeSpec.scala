package com.evolutiongaming.fp

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import scala.collection.immutable.{List => ScalaList}

class ApplicativeSpec extends FreeSpec with GeneratorDrivenPropertyChecks {
  import com.evolutiongaming.fp.Applicative._
  import com.evolutiongaming.fp.ExactlyOne._
  import com.evolutiongaming.fp.List
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  "ExactlyOne instance" -{
    "pure == ExactlyOne" in {
      forAll { (x: Int) =>
        assert(exactlyOneApplicative.pure(x) === ExactlyOne(x))
      }
    }
    "Applying within ExactlyOne" in {
      assert(exactlyOneApplicative.apply(ExactlyOne((x: Int) => x + 10))(ExactlyOne(8)) === ExactlyOne(18))
    }
  }

  "List instance" -{
    "pure" in {
      forAll { (x: Int) =>
        assert(listApplicative.pure(x) === x :: Nil)
      }
    }
    "apply" in {
      assert(listApplicative.apply(((x: Int) => x + 1) :: ((x: Int) => x * 2) :: Nil)(listScala(ScalaList(1, 2, 3))) === listScala(ScalaList(2,3,4,2,4,6)))
    }
  }

  "<$$>" -{
    "ExactlyOne" in {
      assert(exactlyOneApplicative.fmap((x: Int) => x + 1)(ExactlyOne(2)) === ExactlyOne(3))
    }
    "empty List" in {
      assert(listApplicative.fmap((x: Int) => x + 1)(Nil: List[Int]) === Nil)
    }
    "List" in {
      assert(listApplicative.fmap((x: Int) => x +  1)(listScala(ScalaList(1, 2, 3))) === listScala(ScalaList(2,3,4)))
    }
  }

  "Optional instance" -{
    "pure" in {
      forAll { (x: Int) =>
        assert(optionalApplicative.pure(x) === Full(x))
      }
    }
    "Full <*> Full" in {
      assert(optionalApplicative.apply(Full((x: Int) => x + 8))(Full(7)) === Full(15))
    }
    "Empty <*> Full" in {
      assert(optionalApplicative.apply(Empty)(Full("tilt")) === Empty)
    }
    "Full <*> Empty" in {
      assert(optionalApplicative.apply(Full((x: Int) => x + 8))(Empty) === Empty)
    }
  }

  "Function instance" -{
    def twoIntsSum(x: Int)(y: Int) = x + y
    def twoIntsProduct(x: Int)(y: Int) = x * y

    "addition" in {
      assert(functionApplicative[Int].apply(twoIntsSum)((x: Int) => x + 10)(3) === 16)
    }
    "more addition" in {
      assert(functionApplicative[Int].apply(twoIntsSum)((x: Int) => x + 5)(3) === 11)
    }
    "even more addition" in {
      assert(functionApplicative[Int].apply(twoIntsSum)((x: Int) => x + 5)(1) === 7)
    }
    "addition and multiplication" in {
      assert(functionApplicative[Int].apply(twoIntsProduct)((x: Int) => x + 10)(3) === 39)
    }
    "more addition and multiplcation" in {
      assert(functionApplicative[Int].apply(twoIntsProduct)((x: Int) => x + 2)(3) === 15)
    }
    "pure" in {
      forAll { (input: (Int, Int)) =>
        val (x, y) = input
        assert(functionApplicative[Int].pure(x)(y) === x)
      }
    }
  }

  "lift2" -{
    def twoIntsSum(x: Int)(y: Int) = x + y

    "+ over ExactlyOne" in {
      assert(exactlyOneApplicative.lift2(twoIntsSum)(ExactlyOne(7))(ExactlyOne(8)) === ExactlyOne(15))
    }
    "+ over List" in {
      assert(listApplicative.lift2(twoIntsSum)(listScala(ScalaList(1,2,3)))(listScala(ScalaList(4,5))) === listScala(ScalaList(5,6,6,7,7,8)))
    }
    "+ over Optional - all full" in {
      assert(optionalApplicative.lift2(twoIntsSum)(Full(7))(Full(8)) === Full(15))
    }
    "+ over Optional - first Empty" in {
      assert(optionalApplicative.lift2(twoIntsSum)(Empty: Optional[Int])(Full(8)) === Empty)
    }
    "+ over Optional - second Empty" in {
      assert(optionalApplicative.lift2(twoIntsSum)(Full(7))(Empty: Optional[Int]) === Empty)
    }
    "+ over functions" in {
      assert(functionApplicative[List[Int]].lift2(twoIntsSum)(List.length)(sum)(listScala(ScalaList(4,5,6))) === 18)
    }
  }

  "lift3" -{
    def threeIntsSum(x: Int)(y: Int)(z: Int) = x + y + z

    "+ over ExactlyOne" in {
      assert(exactlyOneApplicative.lift3(threeIntsSum)(ExactlyOne(7))(ExactlyOne(8))(ExactlyOne(9)) === ExactlyOne(24))
    }
    "+ over List" in {
      val a = listScala(ScalaList(1,2,3))
      val b = listScala(ScalaList(4,5))
      val c = listScala(ScalaList(6,7,8))
      val expected = listScala(ScalaList(11,12,13,12,13,14,12,13,14,13,14,15,13,14,15,14,15,16))
      assert(listApplicative.lift3(threeIntsSum)(a)(b)(c) === expected)
    }
    "+ over Optional" in {
      assert(optionalApplicative.lift3(threeIntsSum)(Full(7))(Full(8))(Full(9)) === Full(24))
    }
    "+ over Optional - third Empty" in {
      assert(optionalApplicative.lift3(threeIntsSum)(Full(7))(Full(8))(Empty: Optional[Int]) === Empty)
    }
    "+ over Optional - first Empty" in {
      assert(optionalApplicative.lift3(threeIntsSum)(Empty: Optional[Int])(Full(8))(Full(9)) === Empty)
    }
    "+ over Optional - first and second Empty" in {
      assert(optionalApplicative.lift3(threeIntsSum)(Empty: Optional[Int])(Empty: Optional[Int])(Full(9)) === Empty)
    }
    "+ over functions" in {
      assert(functionApplicative[List[Int]].lift3(threeIntsSum)(List.length)(sum)(product)(listScala(ScalaList(4,5,6))) === 138)
    }
  }

  "lift4" -{
    def fourIntsSum(x: Int)(y: Int)(z: Int)(w: Int) = x + y + z + w

    "+ over ExactlyOne" in {
      assert(exactlyOneApplicative.lift4(fourIntsSum)(ExactlyOne(7))(ExactlyOne(8))(ExactlyOne(9))(ExactlyOne(10)) === ExactlyOne(34))
    }
    "+ over List" in {
      val a = listScala(ScalaList(1,2,3))
      val b = listScala(ScalaList(4,5))
      val c = listScala(ScalaList(6,7,8))
      val d = listScala(ScalaList(9,10))
      val expected = listScala(ScalaList(20,21,21,22,22,23,21,22,22,23,23,24,21,22,22,23,23,24,22,23,23,24,24,25,22,23,23,24,24,25,23,24,24,25,25,26))
      assert(listApplicative.lift4(fourIntsSum)(a)(b)(c)(d) === expected)
    }
    "+ over Optional" in {
      assert(optionalApplicative.lift4(fourIntsSum)(Full(7))(Full(8))(Full(9))(Full(10)) === Full(34))
    }
    "+ over Optional - third Empty" in {
      assert(optionalApplicative.lift4(fourIntsSum)(Full(7))(Full(8))(Empty: Optional[Int])(Full(10)) === Empty)
    }
    "+ over Optional - first Empty" in {
      assert(optionalApplicative.lift4(fourIntsSum)(Empty: Optional[Int])(Full(8))(Full(9))(Full(10)) === Empty)
    }
    "+ over Optional - first and second Empty" in {
      assert(optionalApplicative.lift4(fourIntsSum)(Empty: Optional[Int])(Empty: Optional[Int])(Full(9))(Full(10)) === Empty)
    }
    "+ over functions" in {
      assert(functionApplicative[List[Int]].lift4(fourIntsSum)(List.length)(sum)(product)(x => sum(filter((y: Int) => y % 2 == 0)(x)))(listScala(ScalaList(4,5,6))) === 148)
    }
  }

  "rightApply" -{
    "*> over List" in {
      assert(listApplicative.rapply(listScala(ScalaList(1,2,3)))(listScala(ScalaList(4,5,6))) === listScala(ScalaList(4,5,6,4,5,6,4,5,6)))
    }
    "*> over List again" in {
      assert(listApplicative.rapply(listScala(ScalaList(1,2)))(listScala(ScalaList(4,5,6))) === listScala(ScalaList(4,5,6,4,5,6)))
    }
    "another *> over List" in {
      assert(listApplicative.rapply(listScala(ScalaList(1,2,3)))(listScala(ScalaList(4,5))) === listScala(ScalaList(4,5,4,5,4,5)))
    }
    "*> over Optional" in {
      assert(optionalApplicative.rapply(Full(7))(Full(8)) === Full(8))
    }
    "*> over List property" in {
      forAll { (input: (Int, Int, Int, Int, Int, Int)) =>
        val (a, b, c, x, y, z) = input
        val l1: List[Int] = listScala(ScalaList(a, b, c))
        val l2: List[Int] = listScala(ScalaList(x, y, z))
        assert(listApplicative.rapply(l1)(l2) === listScala(ScalaList(x, y, z, x, y, z, x, y, z)))
      }
    }
    "*> over Optional property" in {
      forAll { (input: (Int, Int)) =>
        val (x, y) = input
        assert(optionalApplicative.rapply(Full(x))(Full(y)) === Full(y))
      }
    }
  }

  "leftApply" -{
    "<* over List" in {
      val a = 1 :: 2 :: 3 :: Nil
      val b = 4 :: 5 :: 6 :: Nil
      assert(listApplicative.lapply(a)(b) === listScala(ScalaList(1,1,1,2,2,2,3,3,3)))
    }
    "another <* over List" in {
      val a = 1 :: 2 :: Nil
      val b = 4 :: 5 :: 6 :: Nil
      assert(listApplicative.lapply(a)(b) === listScala(ScalaList(1,1,1,2,2,2)))
    }
    "Yet another <* over List" in {
      val a = 1 :: 2 :: 3 :: Nil
      val b = 4 :: 5 :: Nil
      assert(listApplicative.lapply(a)(b) === listScala(ScalaList(1,1,2,2,3,3)))
    }
    "<* over Optional" in {
      assert(optionalApplicative.lapply(Full(7))(Full(8)) === Full(7))
    }
    "<* over List property" in {
      forAll { (input: (Int, Int, Int, Int, Int, Int)) =>
        val (x, y, z, a, b, c) = input
        val l1 = x :: y :: z :: Nil
        val l2 = a :: b :: c :: Nil
        assert(listApplicative.lapply(l1)(l2) === listScala(ScalaList(x, x, x, y, y, y, z, z, z)))
      }
    }
    "<* over Optional property" in {
      forAll { (input: (Int, Int)) =>
        val (x, y) = input
        assert(optionalApplicative.lapply(Full(x))(Full(y)) === Full(x))
      }
    }
  }

  "sequence" -{
    "ExactlyOne" in {
      val list: List[ExactlyOne[Int]] = listScala(ScalaList(ExactlyOne(7), ExactlyOne(8), ExactlyOne(9)))
      assert(sequence(list) === ExactlyOne(listScala(ScalaList(7, 8, 9))))
    }
    "List" in {
      val list: List[List[Int]] = (1 :: 2 :: 3 :: Nil) :: (1 :: 2 :: Nil) :: Nil
      assert(sequence(list) === listScala(ScalaList(ScalaList(1,1), ScalaList(1,2), ScalaList(2,1), ScalaList(2,2), ScalaList(3,1), ScalaList(3,2)).map(x => listScala(x))))
    }
    "Optional with an empty" in {
      val list: List[Optional[Int]] = Full(7) :: Empty :: Nil
      assert(sequence(list) === Empty)
    }
    "Optional" in {
      val list: List[Optional[Int]] = Full(7) :: Full(8) :: Nil
      assert(sequence(list) === Full(listScala(ScalaList(7,8))))
    }
    "(=>)" in {
      val list: List[Int => Int] = ((x: Int) => x * 10) :: ((x: Int) => x + 2) :: Nil
      val f: Int => List[Int] = sequence[Int, Int => ?](list)
      assert(f(6) === listScala(ScalaList(60, 8)))
    }
  }

  "replicateA" -{
    "ExactlyOne" in {
      assert(replicateA(4, ExactlyOne("hi")) === ExactlyOne(listScala(ScalaList("hi","hi","hi","hi"))))
    }
    "Optional - Full" in {
      assert(replicateA(4, Full("hi"): Optional[String]) === Full(listScala(ScalaList("hi", "hi", "hi", "hi"))))
    }
    "Optional - Empty" in {
      assert(replicateA(4, Empty) === Empty)
    }
    "(=>)" in {
      def mult: Int => Int = x => x * 2
      def f: Int => List[Int] = replicateA[Int, Int => ?](4, mult)
      assert(f(5) === listScala(ScalaList(10,10,10,10)))
    }
    "List" in {
      val expected = listScala(ScalaList("aaa","aab","aac","aba","abb","abc","aca","acb","acc",
      "baa","bab","bac","bba","bbb","bbc","bca","bcb","bcc",
      "caa","cab","cac","cba","cbb","cbc","cca","ccb","ccc").map(x => listScala(x.toList)))
      assert(replicateA(3, 'a' :: 'b' :: 'c' :: Nil) === expected)
    }
  }

  "filtering" -{
    "ExactlyOne" in {
      assert(filtering((x: Int) => ExactlyOne(x % 2 == 0), 4 :: 5 :: 6 :: Nil) === ExactlyOne(listScala(ScalaList(4, 6))))
      //filtering (ExactlyOne . even) (4 :. 5 :. 6 :. Nil) @?= ExactlyOne (listh [4,6])
    }
    "Optional - all true" in {
      assert(filtering((x: Int) => if (x > 13) Empty else Full(x <= 7), 4 :: 5 :: 6 :: Nil) === Full(listScala(ScalaList(4,5,6))))
    }
    "Optional - some false" in {
      assert(filtering((x: Int) => if (x > 13) Empty else Full(x <= 7), 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: Nil) === Full(listScala(ScalaList(4,5,6,7))))
    }
    "Optional - some empty" in {
      assert(filtering((x: Int) => if (x > 13) Empty else Full(x <= 7), 4 :: 5 :: 6 :: 13 :: 14 :: Nil) === Empty)
    }
    "(=>)" in {
      val list: List[Int] = 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: 10 :: 11 :: 12 :: Nil
      val f: Int => Int => Boolean = a => b => a > b
      val ff: Int => List[Int] = filtering[Int, Int => ?](f, list)
      assert(ff(8) === listScala(ScalaList(9,10,11,12)))
    }
    "List" in {

      val expected = listScala(ScalaList(ScalaList(1,2,3), ScalaList(1,2,3), ScalaList(1,2,3), ScalaList(1,2,3),
        ScalaList(1,2,3), ScalaList(1,2,3), ScalaList(1,2,3), ScalaList(1,2,3)).map(x => listScala(x)))
      def f: Int => List[Boolean] = x => true :: true :: Nil
      assert(filtering(f, 1 :: 2 :: 3 :: Nil) === expected)
    }
  }

}
