package com.evolutiongaming.fp

import org.scalatest._

class OptionalSpec extends FreeSpec with Matchers {
  import com.evolutiongaming.fp.Optional._

  "mapOptional" - {
    "Empty" in {
      assert(mapOptional((x: Int) => x + 1, Empty) === Empty)
    }
    "Full" in {
      assert(mapOptional((x: Int) => x + 1, Full(8)) === Full(9))
    }
  }

  "bindOptional" - {
    def evenDecOddInc(n: Int) = if (n % 2 == 0) Full(n - 1) else Full(n + 1)

    "Empty" in {
      assert(bindOptional((x: Int) => Full(x), Empty) === Empty)
    }
    "even dec, odd inc, even input" in {
      assert(bindOptional(evenDecOddInc, Full(8)) === Full(7))
    }
    "even dec, odd inc, odd input" in {
      assert(bindOptional(evenDecOddInc, Full(9)) === Full(10))
    }
  }

  "??" -{
    "Full" in {
      assert((Full(8) ?? 99) === 8)
    }
    "Empty" in {
      assert((Empty ?? 99) === 99)
    }
  }

  "<+>" -{
    "first Full" in {
      assert((Full(8) <+> Empty) === Full(8))
    }
    "both Full" in {
      assert((Full(8) <+> Full(9)) === Full(8))
    }
    "first Empty" in {
      assert((Empty <+> Full(9)) === Full(9))
    }
    "both empty" in {
      assert((Empty <+> Empty) === Empty)
    }
  }

}

