package com.evolutiongaming.fp

import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalatest.FreeSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class ValidationSpec extends FreeSpec with GeneratorDrivenPropertyChecks {

  import com.evolutiongaming.fp.Validation._

  "isError" -{
    "Error" in {
      assert(isError(Error("message")) === true)
    }
    "Value" in {
      assert(isError(Value(7)) === false)
    }
    "Error is not Value" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(isError(v) !== isValue(v))
      }
    }
  }

  "isValue" -{
    "Error" in {
      assert(isValue(Error("message")) === false)
    }
    "Value" in {
      assert(isValue(Value(666)) === true)
    }
    "Value is not Error" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(isValue(v) !== isError(v))
      }
    }
  }

  "mapValidation" -{
    "Error" in {
      assert(mapValidation((x: Int) => x + 10)(Error("message")) === Error("message"))
    }
    "Value" in {
      assert(mapValidation((x: Int) => x + 10)(Value(7)) === Value(17))
    }
    "identity" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(mapValidation(identity[Int])(v) === v)
      }
    }
  }

  "bindValidation" -{
    "Error" in {
      assert(bindValidation((x: Int) => if (x % 2 == 0) Value(x + 10) else Error("odd"))(Error("message")) === Error("message"))
    }
    "Value to Error" in {
      assert(bindValidation((x: Int) => if (x % 2 == 0) Value(x + 10) else Error("odd"))(Value(7)) === Error("odd"))
    }
    "Value" in {
      assert(bindValidation((x: Int) => if (x % 2 == 0) Value(x + 10) else Error("odd"))(Value(8)) === Value(18))
    }
    "identity" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(bindValidation((x: Int) => Value(x))(v) === v)
      }
    }
  }

  "valueOr" -{
    "Error" in {
      assert(valueOr(Error("message"))(3) === 3)
    }
    "Value" in {
      assert(valueOr(Value(7))(3) === 7)
    }
    "prop" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(isValue(v) || valueOr(v)(7) === 7)
      }
    }
  }

  "errorOr" -{
    "Error" in {
      assert(errorOr(Error("message"))("q") === "message")
    }
    "Value" in {
      assert(errorOr(Value(7))("q") === "q")
    }
    "prop" in {
      forAll(genValidation) { (v: Validation[Int]) =>
        assert(isError(v) || errorOr(v)("msg") === "msg")
      }
    }
  }





  def genValidation: Gen[Validation[Int]] =
    arbitrary[Int].map(x => if (scala.util.Random.nextInt() % 2 == 0) Value(x) else Error("err"))
}
