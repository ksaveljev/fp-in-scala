package com.evolutiongaming.fp

import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalatest.FreeSpec
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scala.collection.immutable.{List => ScalaList}

class StateTSpec extends FreeSpec with GeneratorDrivenPropertyChecks {

  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Monad._
  import com.evolutiongaming.fp.Optional._
  import com.evolutiongaming.fp.State._
  import com.evolutiongaming.fp.StateT._

  "functorTest" -{
    "<$>" in {
      val F: Functor[StateT[Int, List, ?]] = stateTFunctor

      val st = new StateT[Int, List, Int] {
        def run(s: Int): List[(Int, Int)] = (2, s) :: Nil
      }

      assert(F.fmap((x: Int) => x + 1)(st).run(0) === (3, 0) :: Nil)
    }
  }

  "Applicative" -{
    "List (pure)" in {
      val F: Applicative[StateT[Int, List, ?]] = stateTApplicative
      assert(F.pure(2).run(0) === (2, 0) :: Nil)
    }
    "List (<*>)" in {
      val F: Applicative[StateT[Int, List, ?]] = stateTApplicative
      assert(F.apply(F.pure((x: Int) => x + 2))(F.pure(2)).run(0) === (4, 0) :: Nil)
    }
    "Optional" in {
      /*
      let st = StateT (\s -> Full ((+2), s P.++ [1])) <*> (StateT (\s -> Full (2, s P.++ [2])))
      in runStateT st [0] @?= Full (4,[0,1,2])
       */
      val F: Applicative[StateT[ScalaList[Int], Optional, ?]] = stateTApplicative
      val s1 = new StateT[ScalaList[Int], Optional, Int] {
        def run(s: ScalaList[Int]): Optional[(Int, ScalaList[Int])] = Full(2, s ++ ScalaList(1))
      }
      val s2 = new StateT[ScalaList[Int], Optional, Int => Int] {
        def run(s: ScalaList[Int]): Optional[(Int => Int, ScalaList[Int])] = Full((_ + 2, s ++ ScalaList(2)))
      }
      assert(F.apply(s2)(s1).run(ScalaList(0)) === Full((4, ScalaList(0, 1, 2))))
    }
    "List" in {
      val F: Applicative[StateT[ScalaList[Int], List, ?]] = stateTApplicative
      val st1 = new StateT[ScalaList[Int], List, Int] {
        def run(s: ScalaList[Int]): List[(Int, ScalaList[Int])] = (2, s ++ ScalaList(1)) :: Nil
      }
      val st2 = new StateT[ScalaList[Int], List, Int => Int] {
        def run(s: ScalaList[Int]): List[(Int => Int, ScalaList[Int])] = ((x: Int) => x + 2, s ++ ScalaList(2)) :: ((x: Int) => x + 3, s ++ ScalaList(2)) :: Nil
      }
      assert(F.apply(st2)(st1).run(ScalaList(0)) === (4, ScalaList(0, 1, 2)) :: (5, ScalaList(0, 1, 2)) :: Nil)
    }
  }

  "Monad" -{
    val F: Monad[StateT[Int, List, ?]] = stateTMonad

    "bind const" in {
      def s(n: Int): StateT[Int, List, Unit] = new StateT[Int, List, Unit] {
        def run(s: Int): List[(Unit, Int)] = ((), n) :: Nil
      }
      assert(F.flatMap((_: Unit) => s(2))(s(1)).run(0) === ((), 2) :: Nil)
    }
    "modify" in {
      def modify(f: Int => Int) = new StateT[Int, List, Unit] {
        def run(s: Int): List[(Unit, Int)] = listMonad.pure(((), f(s)))
      }
      assert(F.flatMap((_: Unit) => modify(_ * 2))(modify(_ + 1)).run(7) === ((), 16) :: Nil)
    }
  }

  "stateS Test" -{
    "stateS" in {
      assert(stateS((s: Int) => put(1).run(s)).run(0) === ExactlyOne((), 1))
    }
  }

  "runStateS Test" -{
    "runStateS" in {
      assert(runStateS(stateS((s: Int) => put(1).run(s)))(0) === ((), 1))
    }
  }

  "getT Test" -{
    "getT" in {
      assert(getT[Int, List].run(3) === (3, 3) :: Nil)
    }
  }

  "putT Test" -{
    "putT" in {
      assert(putT[Int, List](2).run(0) === ((), 2) :: Nil)
    }
  }

  "distinctS Test" -{
    "distinctS" in {
      val F: Monad[List] = implicitly
      forAll(genIntList) { list =>
        assert(distinctS(list) === distinctS(F.flatMap((x: Int) => x :: x :: Nil)(list)))
      }
    }
  }

  "distinctF" -{
    "Full case" in {
      val blah = distinctF(listScala(ScalaList(1,2,3,2,1)))
      assert(distinctF(listScala(ScalaList(1,2,3,2,1))) === Full(listScala(ScalaList(1,2,3))))
    }
    "Empty case" in {
      assert(distinctF(listScala(ScalaList(1,2,3,2,1,101))) === Empty)
    }
  }

  "optionalTFunctor Test" -{
    "(<$>) for OptionalT" in {
      val F: Functor[OptionalT[List, ?]] = optionalTFunctor[List]
      val optional = new OptionalT[List, Int] {
        def run: List[Optional[Int]] = Full(1) :: Empty :: Nil
      }
      assert(F.fmap((x: Int) => x + 1)(optional).run === Full(2) :: Empty :: Nil)
    }
  }

  "optionalTApplicative Test" -{
    "(<*>) for OptionalT" in {
      val F: Applicative[OptionalT[List, ?]] = optionalTApplicative[List]
      val opt1 = new OptionalT[List, Int] {
        def run: List[Optional[Int]] = Full(1) :: Empty :: Nil
      }
      val opt2 = new OptionalT[List, Int => Int] {
        def run: List[Optional[Int => Int]] = Full((x: Int) => x + 1) :: Full((x: Int) => x + 2) :: Nil
      }
      assert(F.apply(opt2)(opt1).run === Full(2) :: Empty :: Full(3) :: Empty :: Nil)
    }
  }

  "optionalTMonad Test" -{
    "(=<<) for OptionalT" in {
      val F: Monad[OptionalT[List, ?]] = optionalTMonad
      val opt1 = new OptionalT[List, Int] {
        def run: List[Optional[Int]] = Full(1) :: Empty :: Nil
      }
      def opt2(a: Int) = new OptionalT[List, Int] {
        def run: List[Optional[Int]] = Full(a + 1) :: Full(a + 2) :: Nil
      }
      assert(F.flatMap(opt2)(opt1).run === Full(2) :: Full(3) :: Empty :: Nil)
    }
  }

  "loggerFunctor Test" -{
    "(<$>) for Logger" in {
      val F: Functor[Logger[Int, ?]] = implicitly
      assert(F.fmap((x: Int) => x + 3)(Logger(1 :: 2 :: Nil, 3)) === Logger(1 :: 2 :: Nil, 6))
      //(+3) <$> Logger (1 :. 2 :. Nil) 3 @?= Logger (1 :. 2 :. Nil) 6
    }
  }

  "Logger Applicative" -{
    val F: Applicative[Logger[Int, ?]] = implicitly

    "pure" in {
      assert(F.pure("table") === Logger(Nil, "table"))
    }
    "<*>" in {
      assert(F.apply(Logger(1 :: 2 :: Nil, (x: Int) => x + 7))(Logger(3 :: 4 :: Nil, 3)) === Logger(1 :: 2 :: 3 :: 4 :: Nil, 10))
      //Logger (1:.2:.Nil) (+7) <*> Logger (3:.4:.Nil) 3 @?= Logger (1:.2:.3:.4:.Nil) 10
    }
  }

  "loggerMonad Test" -{
    "(>>=) for Logger" in {
      val F: Monad[Logger[Int, ?]] = implicitly
      assert(F.flatMap((a: Int) => Logger(4 :: 5 :: Nil, a + 3))(Logger(1 :: 2 :: Nil, 3)) === Logger(1 :: 2 :: 4 :: 5 :: Nil, 6))
      //((\a -> Logger (4:.5:.Nil) (a+3)) =<< Logger (1:.2:.Nil) 3) @?= Logger (1:.2:.4:.5:.Nil) 6
    }
  }

  "log1Test" -{
    "log1" in {
      assert(log1(1, 2) === Logger(1 :: Nil, 2))
    }
  }

  "distinctG" -{
    "Full case" in {
      val expected = Logger("even number: 2" :: "even number: 2" :: "even number: 6" :: Nil, Full(1 :: 2 :: 3 :: 6 :: Nil))
      assert(distinctG(1 :: 2 :: 3 :: 2 :: 6 :: Nil) === expected)
    }
    "Empty case" in {
      val expected = Logger("even number: 2" :: "even number: 2" :: "even number: 6" :: "aborting > 100: 106" :: Nil, Empty)
      assert(distinctG(1 :: 2 :: 3 :: 2 :: 6 :: 106 :: Nil) === expected)
    }
  }











  def genList[A: Arbitrary]: Gen[List[A]] =
    arbitrary[ScalaList[A]].map((v: ScalaList[A]) => v.foldRight(Nil: List[A])((a, b) => a :: b))

  def genIntList: Gen[List[Int]] = genList[Int]
}
