package com.evolutiongaming.fp

import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalatest._
import org.scalatest.prop._
import scala.collection.immutable.{List => ScalaList}

class ListSpec extends FreeSpec with GeneratorDrivenPropertyChecks with Matchers {
  import com.evolutiongaming.fp.List
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional
  import com.evolutiongaming.fp.Optional._

  val testList: List[Int] = {
    def gen(x: Int, n: Int): List[Int] = if (x > n) Nil else x :: gen(x+1, n)
    gen(0, 1000)
  }

  "headOr" -{
    "headOr on non-empty list" in {
      assert(headOr(3)(1 :: 2 :: Nil) === 1)
    }
    "headOr on empty list" in {
      assert(headOr(3)(Nil) === 3)
    }
    "headOr on infinity always 0" in {
      forAll { (x: Int) =>
        assert(headOr(x)(infinity) === 0)
      }
    }
    "headOr on empty list always the default" in {
      forAll { (x: Int) =>
        assert(headOr(x)(Nil) === x)
      }
    }
  }

  "product" -{
    "product of empty list" in {
      assert(product(Nil) === 1)
    }
    "product of 1..3" in {
      assert(product(1 :: 2 :: 3 :: Nil) === 6)
    }
    "product of 1..4" in {
      assert(product(1 :: 2 :: 3 :: 4 :: Nil) === 24)
    }
  }

  "sum" -{
    "sum 1..3" in {
      assert(sum(1 :: 2 :: 3 :: Nil) === 6)
    }
    "sum 1..4" in {
      assert(sum(1 :: 2 :: 3 :: 4 :: Nil) === 10)
    }
    "subtracting each element in a list from its sum is always 0" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(foldLeft((a: Int, b: Int) => a - b, sum(list), list) === 0)
      }
    }
  }

  "length" -{
    "length 1..3" in {
      assert(List.length(1 :: 2 :: 3 :: Nil) === 3)
    }
    "summing a list of 1s is equal to its length" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(scalaList(list).length === List.length(list))
      }
    }
  }

  "map" -{
    "add 10 on list" in {
      assert(map((x: Int) => x + 10)(1 :: 2 :: 3 :: Nil) === (11 :: 12 :: 13 :: Nil))
    }
    "headOr after map" in {
      forAll { (x: Int) =>
        assert(headOr(x)(map((x: Int) => x + 1)(infinity)) === 1)
      }
    }
    "map id is id" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(map(identity[Int])(list) === list)
      }
    }
  }

  "filter" -{
    "filter even" in {
      assert(filter((x: Int) => x % 2 == 0)(1 :: 2 :: 3 :: 4 :: 5 :: Nil) === (2 :: 4 :: Nil))
    }
    "filter (const True) is identity (headOr)" in {
      forAll { (x: Int) =>
        assert(headOr(x)(filter[Int](_ => true)(testList)) === 0)
      }
    }
    "filter (const True) is identity" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(filter[Int](_ => true)(list) === list)
      }
    }
    "filter (const False) is the empty list" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(filter[Int](_ => false)(list) === Nil)
      }
    }
  }

  "(++:)" -{
    "(1..6)" in {
      assert((1 :: 2 :: 3 :: Nil) ++: (4 :: 5 :: 6 :: Nil) === listScala(ScalaList(1, 2, 3, 4, 5, 6)))
    }
    "append empty to infinity" in {
      forAll { (x: Int) =>
        assert(headOr(x)(Nil ++: infinity) === 0)
      }
    }
    "append anything to infinity" in {
      forAll(genIntAndList) { (input: (Int, List[Int])) => // (Int, List[Int])
        assert(headOr(input._1)(input._2 ++: infinity) === headOr(0)(input._2))
      }
    }
    "associativity" in {
      forAll(genThreeLists) { (input: (List[Int], List[Int], List[Int])) => // (List[Int], List[Int], List[Int])
        val (x, y, z) = input
        assert((x ++: y) ++: z === x ++: (y ++: z))
      }
    }
    "append to empty list" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(list ++: Nil === list)
      }
    }
  }

  "flatten" -{
    "(1..9)" in {
      assert(flatten((1 :: 2 :: 3 :: Nil) :: (4 :: 5 :: 6 :: Nil) :: (7 :: 8 :: 9 :: Nil) :: Nil) === listScala(ScalaList(1,2,3,4,5,6,7,8,9)))
    }
    "flatten (infinity :: y)" in {
      forAll(genIntAndList) { input => // (Int, List[Int])
        assert(headOr(input._1)(flatten(infinity :: input._2 :: Nil)) === 0)
      }
    }
    "flatten (y :: infinity)" in {
      forAll(genIntAndList) { input => // (Int, List[Int])
        assert(headOr(input._1)(flatten (input._2 :: infinity :: Nil)) == headOr(0)(input._2))
      }
    }
    "sum of lengths == length of flattened" in {
      forAll(genListOfLists) { (list: List[List[Int]]) =>
        assert(sum(map(List.length)(list)) === List.length(flatten(list)))
      }
    }
  }

  "flatMap" -{
    "lists of Integer" in {
      assert(flatMap((x: Int) => x :: x + 1 :: x + 2 :: Nil)(1 :: 2 :: 3 :: Nil) === listScala(ScalaList(1,2,3,2,3,4,3,4,5)))
    }
    "flatMap id flattens a list of lists" in {
      forAll(genIntAndList) { input => // (Int, List[Int])
        assert(headOr(input._1)(flatMap(identity[List[Int]])(testList :: input._2 :: Nil)) === 0)
      }
    }
    "flatMap id on a list of lists take 2" in {
      forAll(genIntAndList) { input => // (Int, List[Int])
        assert(headOr(input._1)(flatMap(identity[List[Int]])(input._2 :: testList :: Nil)) === headOr(0)(input._2))
      }
    }
    "flatMap id == flatten" in {
      forAll(genListOfLists) { (list: List[List[Int]]) =>
        assert(flatMap(identity[List[Int]])(list) === flatten(list))
      }
    }
  }

  "flattenAgain" -{
    "lists of Integer" in {
      forAll(genListOfLists) { (list: List[List[Int]]) =>
        assert(flatten(list) === flattenAgain(list))
      }
    }
  }

  "seqOptional" -{
    "all Full" in {
      assert(seqOptional(Full(1) :: Full(10) :: Nil) === Full(1 :: 10 :: Nil))
    }
    "empty list" in {
      val empty: List[Optional[Int]] = Nil
      assert(seqOptional(empty) === Full(Nil))
    }
    "contains Empty" in {
      assert(seqOptional(Full(1) :: Full(10) :: Empty :: Nil) === Empty)
    }
    "Empty at head of infinity" in {
      assert(seqOptional(Empty :: map((x: Int) => Full(x))(infinity)) === Empty)
    }
  }

  "find" -{
    "find no matches" in {
      assert(find((x: Int) => x % 2 == 0)(1 :: 3 :: 5 :: Nil) === Empty)
    }
    "empty list" in {
      assert(find((x: Int) => x % 2 == 0)(Nil) === Empty)
    }
    "find only even" in {
      assert(find((x: Int) => x % 2 == 0)(1 :: 2 :: 3 :: 5 :: Nil) === Full(2))
    }
    "find first, not second even" in {
      assert(find((x: Int) => x % 2 == 0)(1 :: 2 :: 3 :: 4 :: 5 :: Nil) === Full(2))
    }
    "find on infinite list" in {
      assert(find((_: Int) => true)(infinity) === Full(0))
    }
  }

  "lengthGT4" -{
    "list of length 3" in {
      assert(lengthGT4(1 :: 3 :: 5 :: Nil) === false)
    }
    "empty list" in {
      assert(lengthGT4(Nil) === false)
    }
    "list of length 5" in {
      assert(lengthGT4(1 :: 2 :: 3 :: 4 :: 5 :: Nil) === true)
    }
    "infinite list" in {
      assert(lengthGT4(infinity) === true)
    }
  }

  "reverse" -{
    "empty list" in {
      assert(reverse(Nil) === (Nil: List[Int]))
    }
    "reverse . reverse on largeList" in {
      assert(take(1, reverse(reverse(largeList))) === (1 :: Nil))
    }
    "reverse then append is same as append then reverse" in {
      forAll(genTwoLists) { input => // (List[Int], List[Int])
        val (x, y) = input
        assert(reverse(x) ++: reverse(y) === reverse (y ++: x))
      }
    }
    "reverse single element list" in {
      forAll(genIntList) { (list: List[Int]) =>
        assert(reverse(list :: Nil) === list :: Nil)
      }
    }
  }






  def genList[A: Arbitrary]: Gen[List[A]] =
    arbitrary[ScalaList[A]].map((v: ScalaList[A]) => v.foldRight(Nil: List[A])((a, b) => a :: b))

  def genIntList: Gen[List[Int]] = genList[Int]

  def genIntAndList: Gen[(Int, List[Int])] =
    arbitrary[(Int, ScalaList[Int])].map(v => (v._1, listScala(v._2)))

  def genTwoLists: Gen[(List[Int], List[Int])] =
    for {
      x <- genIntList
      y <- genIntList
    } yield (x, y)

  def genThreeLists: Gen[(List[Int], List[Int], List[Int])] =
    for {
      x <- genIntList
      y <- genIntList
      z <- genIntList
    } yield (x, y, z)

  def genListOfLists: Gen[List[List[Int]]] =
    genIntList.map((list: List[Int]) => map((v: Int) => listScala(ScalaList(v)))(list))
}
