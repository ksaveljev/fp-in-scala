package com.evolutiongaming.fp

import org.scalatest.FreeSpec

class MonadSpec extends FreeSpec {
  import com.evolutiongaming.fp.ExactlyOne._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Monad._
  import com.evolutiongaming.fp.Optional._

  "<**>" -{
    def twoIntsSum(x: Int)(y: Int) = x + y
    def twoIntsProduct(x: Int)(y: Int) = x * y

    "ExactlyOne" in {
      assert(exactlyOneMonad.apply(ExactlyOne((x: Int) => x + 10))(ExactlyOne(8)) === ExactlyOne(18))
    }
    "List" in {
      assert(listMonad.apply(((x: Int) => x + 1) :: ((x: Int) => x * 2) :: Nil)(1 :: 2 :: 3 :: Nil) === 2 :: 3 :: 4 :: 2 :: 4 :: 6 :: Nil)
    }
    "Optional" in {
      assert(optionalMonad.apply(Full((x: Int) => x + 8))(Full(7)) === Full(15))
    }
    "Optional - empty function" in {
      assert(optionalMonad.apply(Empty)(Full(7)) === Empty)
    }
    "Optional - empty value" in {
      assert(optionalMonad.apply(Full((x: Int) => x + 8))(Empty) === Empty)
    }
    "(=>) 1" in {
      assert(functionMonad[Int].apply(twoIntsSum)((x: Int) => x + 10)(3) === 16)
    }
    "(->) 2" in {
      assert(functionMonad[Int].apply(twoIntsSum)((x: Int) => x + 5)(3) === 11)
    }
    "(->) 3" in {
      assert(functionMonad[Int].apply(twoIntsSum)((x: Int) => x + 5)(1) === 7)
    }
    "(->) 4" in {
      assert(functionMonad[Int].apply(twoIntsProduct)((x: Int) => x + 10)(3) === 39)
    }
    "(->) 5" in {
      assert(functionMonad[Int].apply(twoIntsProduct)((x: Int) => x + 2)(3) === 15)
    }
  }

  ">>=" -{
    "(>>=) for ExactlyOne" in {
      assert(exactlyOneMonad.flatMap((x: Int) => ExactlyOne(x + 1))(ExactlyOne(2)) === ExactlyOne(3))
    }
    "(>>=) for List" in {
      assert(listMonad.flatMap((n: Int) => n :: n :: Nil)(1 :: 2 :: 3 :: Nil) === 1 :: 1 :: 2 :: 2 :: 3 :: 3 :: Nil)
    }
    "(>>=) for Optional" in {
      assert(optionalMonad.flatMap((n: Int) => Full(n + n))(Full(7)) === Full(14))
    }
    "(>>=) for (=>)" in {
      assert(functionMonad[Int].flatMap((x: Int) => (y: Int) => x * y)((x: Int) => x + 10)(7) === 119)
    }
  }

  "join" -{
    "List" in {
      assert(join((1 :: 2 :: 3 :: Nil) :: (1 :: 2 :: Nil) :: Nil) === 1 :: 2 :: 3 :: 1 :: 2 :: Nil)
    }
    "Optional with Empty" in {
      assert(join(Full(Empty: Optional[Int])) === Empty)
    }
    "Optional all Full" in {
      assert(join(Full(Full(7)): Optional[Optional[Int]]) === Full(7))
    }
    "(=>)" in {
      val f = join[Int, Int => ?](x => y => x + y)
      assert(f(7) === 14)
    }
  }

  "bindFlipped" -{
    "(=<<)" in {
      val f = bindFlipped[Int, Int, Int => ?](x => y => x * y)(_ + 10)
      assert(f(7) === 119)
    }
  }

  "kleisliComposition" -{
    "(>=>)" in {
      val f = kleisliArrow((n: Int) => (n + 1) :: (n + 2) :: Nil)(n => n :: n :: Nil)
      assert(f(1) === 2 :: 2 :: 3 :: 3 :: Nil)
    }
  }

}
