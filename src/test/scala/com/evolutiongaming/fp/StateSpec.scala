package com.evolutiongaming.fp

import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.arbitrary
import org.scalatest._
import org.scalatest.prop._
import scala.collection.immutable.{List => ScalaList}

class StateSpec extends FreeSpec with GeneratorDrivenPropertyChecks {

  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Monad._
  import com.evolutiongaming.fp.Optional._
  import com.evolutiongaming.fp.State._

  "execTest" -{
    "exec" in {
      forAll { (input: (Function1[Int, (Int, Int)], Int)) =>
        val (f, s) = input
        val state = new State[Int, Int] {
          def run(s: Int): (Int, Int) = f(s)
        }
        assert(exec(state, s) === state.run(s)._2)
      }
    }
  }

  "evalTest" -{
    "eval" in {
      forAll { (input: (Function1[Int, (Int, Int)], Int)) =>
        val (f, s) = input
        val state = new State[Int, Int] {
          def run(s: Int): (Int, Int) = f(s)
        }
        assert(eval(state, s) === state.run(s)._1)
      }
    }
  }

  "getTest" -{
    "get" in {
      assert(get[Int].run(0) === (0, 0))
    }
  }

  "putTest" -{
    "put" in {
      assert(put(1).run(0) === ((),1) )
    }
  }

  "functorTest" -{
    "(<$>)" in {
      val state = new State[Int, Int] {
        def run(s: Int): (Int, Int) = (9, s * 2)
      }
      assert(stateFunctor.fmap((x: Int) => x + 1)(state).run(3) === (10, 6))
    }
  }

  "Applicative" -{
    "pure" in {
      val F: Applicative[State[Int, ?]] = implicitly
      assert(F.pure(2).run(0) === (2, 0))
    }
    "<*>" in {
      val F: Applicative[State[Int, ?]] = implicitly
      assert(F.apply(F.pure((x: Int) => x + 1))(F.pure(0)).run(0) === (1, 0))
    }
    "complicated <*>" in {
      val F: Applicative[State[ScalaList[String], ?]] = implicitly

      val s1 = new State[ScalaList[String], Int => Int] {
        def run(s: ScalaList[String]): (Int => Int, ScalaList[String]) = (_ + 3, s ++ ScalaList("apple"))
      }
      val s2 = new State[ScalaList[String], Int] {
        def run(s: ScalaList[String]): (Int, ScalaList[String]) = (7, s ++ ScalaList("banana"))
      }
      val state: State[ScalaList[String], Int] = F.apply(s1)(s2)
      assert(state.run(ScalaList()) === (10, ScalaList("apple", "banana")))
    }
  }

  "Monad" -{
    val F: Monad[State[Int, ?]] = implicitly

    "(>>=)" in {
      assert(F.flatMap((_: Unit) => put(2))(put(1)).run(0) === ((), 2))
    }
    "(>>=) 2" in {
      def modify(f: Int => Int) = new State[Int, Unit] {
        def run(s: Int): (Unit, Int) = ((), f(s))
      }
      assert(F.flatMap((_: Unit) => modify(_ * 2))(modify(_ + 1)).run(7) === ((), 16))
    }
  }

  "findM" -{
    val F: Monad[State[Int, ?]] = implicitly

    "find 'c' in 'a'..'h'" in {
      def predicate(x: Char): State[Int, Boolean] =
        F.flatMap((s: Int) => F.flatMap((_: Unit) => F.pure(x == 'c'))(put(1 + s)))(get)

      assert(findM[Char, State[Int, ?]](predicate)(listScala(ScalaList('a','b','c','d','e','f','g','h'))).run(0) === (Full('c'), 3))
    }
    "find 'i' in 'a'..'h'" in {
      def predicate(x: Char): State[Int, Boolean] =
        F.flatMap((s: Int) => F.flatMap((_: Unit) => F.pure(x == 'i'))(put(1 + s)))(get)

      assert(findM[Char, State[Int, ?]](predicate)(listScala(ScalaList('a','b','c','d','e','f','g','h'))).run(0) === (Empty, 8))
    }
  }

  "firstRepeat" -{
    "finds repeats" in {
      forAll(genIntList) { list =>
        firstRepeat(list) match {
          case Empty   => assert(scalaList(list).distinct === scalaList(list))
          case Full(x) => assert(length(filter((v: Int) => v == x)(list)) > 1)
        }
      }
    }
    "finds repeats 2" in {
      forAll(genIntList) { list =>
        firstRepeat(list) match {
          case Empty   => true
          case Full(x) =>
            val xs = scalaList(list)
            val (l, r) = xs.span(_ != x)
            val (l2, _) = r.tail.span(_ != x)
            val l3 = l ++ ScalaList(x) ++ l2
            assert(l3.distinct === l3)
        }
      }
    }
  }

  "distinct" -{
    "No repeats after distinct" in {
      forAll(genIntList) { list =>
        assert(firstRepeat(distinct(list)) === Empty)
      }
    }
    "No repeats after distinct 2" in {
      forAll(genIntList) { list =>
        assert(distinct(list) === distinct(listMonad.flatMap((x: Int) => x :: x :: Nil)(list)))
      }
    }
  }

  "isHappy" -{
    "4" in {
      assert(isHappy(4) === false)
    }
    "7" in {
      assert(isHappy(7) === true)
    }
    "42" in {
      assert(isHappy(42) === false)
    }
    "44" in {
      assert(isHappy(44) === true)
    }
  }







  def genList[A: Arbitrary]: Gen[List[A]] =
    arbitrary[ScalaList[A]].map((v: ScalaList[A]) => v.foldRight(Nil: List[A])((a, b) => a :: b))

  def genIntList: Gen[List[Int]] = genList[Int]
}
