package com.evolutiongaming.fp

import org.scalactic.Equality

import scala.collection.immutable.Set

// A `StateT` is a function from a state value `s` to a functor f of (a produced value `a`, and a resulting state `s`).
trait StateT[S, F[_], A] {
  def run(s: S): F[(A, S)]
}

object StateT {

  import com.evolutiongaming.fp.Applicative._
  import com.evolutiongaming.fp.ExactlyOne._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  // Implement the `Functor` instance for @StateT s f@ given a @Functor f@.
  //
  // >>> runStateT ((+1) <$> (pure 2) :: StateT Int List Int) 0
  // [(3,0)]
  implicit def stateTFunctor[S, F[_]](implicit F: Functor[F]): Functor[StateT[S, F, ?]] = ???

  // Implement the `Applicative` instance for @StateT s f@ given a @Monad f@.
  //
  // >>> runStateT (pure 2) 0
  // (2,0)
  //
  // >>> runStateT ((pure 2) :: StateT Int List Int) 0
  // [(2,0)]
  implicit def stateTApplicative[S, F[_]](implicit F: Monad[F]): Applicative[StateT[S, F, ?]] = ???

  // Implement the `Monad` instance for @StateT s f@ given a @Monad f@.
  // Make sure the state value is passed through in `bind`.
  //
  // >>> runStateT ((const $ putT 2) =<< putT 1) 0
  // ((),2)
  //
  // >>> let modify f = StateT (\s -> pure ((), f s)) in runStateT (modify (+1) >>= \() -> modify (*2)) 7
  // ((),16)
  implicit def stateTMonad[S, F[_]](implicit F: Monad[F]): Monad[StateT[S, F, ?]] = ???

  // A `State'` is `StateT` specialised to the `ExactlyOne` functor.
  type StateS[S, A] = StateT[S, ExactlyOne, A]

  // Provide a constructor for `State'` values
  //
  // >>> runStateT (state' $ runState $ put 1) 0
  // ExactlyOne  ((),1)
  def stateS[S, A](f: S => (A, S)): StateS[S, A] = ???

  // Provide an unwrapper for `State'` values.
  //
  // >>> runState' (state' $ runState $ put 1) 0
  // ((),1)
  def runStateS[S, A](ss: StateS[S, A])(s: S): (A, S) = ???

  // Run the `StateT` seeded with `s` and retrieve the resulting state.
  def execT[S, F[_], A](ss: StateT[S, F, A])(s: S)(implicit F: Functor[F]): F[S] = ???

  // Run the `State` seeded with `s` and retrieve the resulting state.
  def execS[S, A](ss: StateS[S, A])(s: S): S = ???

  // Run the `StateT` seeded with `s` and retrieve the resulting value.
  def evalT[S, F[_], A](ss: StateT[S, F, A])(s: S)(implicit F: Functor[F]): F[A] = ???

  // Run the `State` seeded with `s` and retrieve the resulting value.
  def evalS[S, A](ss: StateS[S, A])(s: S): A = ???

  // A `StateT` where the state also distributes into the produced value.
  //
  // >>> (runStateT (getT :: StateT Int List Int) 3)
  // [(3,3)]
  def getT[S, F[_]](implicit F: Applicative[F]): StateT[S, F, S] = ???

  // A `StateT` where the resulting state is seeded with the given value.
  //
  // >>> runStateT (putT 2) 0
  // ((),2)
  //
  // >>> runStateT (putT 2 :: StateT Int List ()) 0
  // [((),2)]
  def putT[S, F[_]](v: S)(implicit F: Monad[F]): StateT[S, F, Unit] = ???

  // Remove all duplicate elements in a `List`.
  //
  // /Tip:/ Use `filtering` and `State'` with a @Data.Set#Set@.
  //
  // prop> distinct' xs == distinct' (flatMap (\x -> x :. x :. Nil) xs)
  def distinctS[A](list: List[A]): List[A] = ???

  // Remove all duplicate elements in a `List`.
  // However, if you see a value greater than `100` in the list,
  // abort the computation by producing `Empty`.
  //
  // /Tip:/ Use `filtering` and `StateT` over `Optional` with a @Data.Set#Set@.
  //
  // >>> distinctF $ listh [1,2,3,2,1]
  // Full [1,2,3]
  //
  // >>> distinctF $ listh [1,2,3,2,1,101]
  // Empty
  def distinctF(list: List[Int]): Optional[List[Int]] = ???

  // An `OptionalT` is a functor of an `Optional` value.
  trait OptionalT[F[_], A] {
    def run: F[Optional[A]]
  }

  // Implement the `Functor` instance for `OptionalT f` given a Functor f.
  //
  // >>> runOptionalT $ (+1) <$> OptionalT (Full 1 :. Empty :. Nil)
  // [Full 2,Empty]
  implicit def optionalTFunctor[F[_]](implicit F: Functor[F]): Functor[OptionalT[F, ?]] = ???

  // Implement the `Applicative` instance for `OptionalT f` given a Applicative f.
  //
  // >>> runOptionalT $ OptionalT (Full (+1) :. Full (+2) :. Nil) <*> OptionalT (Full 1 :. Empty :. Nil)
  // [Full 2,Empty,Full 3,Empty]
  implicit def optionalTApplicative[F[_]](implicit F: Applicative[F]): Applicative[OptionalT[F, ?]] = ???

  // Implement the `Monad` instance for `OptionalT f` given a Monad f.
  //
  // >>> runOptionalT $ (\a -> OptionalT (Full (a+1) :. Full (a+2) :. Nil)) =<< OptionalT (Full 1 :. Empty :. Nil)
  // [Full 2,Full 3,Empty]
  implicit def optionalTMonad[F[_]](implicit F: Monad[F]): Monad[OptionalT[F, ?]] = ???

  // A `Logger` is a pair of a list of log values (`[l]`) and an arbitrary value (`a`).
  case class Logger[L, A](list: List[L], a: A)

  // Implement the `Functor` instance for `Logger
  //
  // >>> (+3) <$> Logger (listh [1,2]) 3
  // Logger [1,2] 6
  implicit def loggerFunctor[L]: Functor[Logger[L, ?]] = ???

  // Implement the `Applicative` instance for `Logger`.
  //
  // >>> pure "table" :: Logger Int P.String
  // Logger [] "table"
  //
  // >>> Logger (listh [1,2]) (+7) <*> Logger (listh [3,4]) 3
  // Logger [1,2,3,4] 10
  implicit def loggerApplicative[L]: Applicative[Logger[L, ?]] = ???

  // Implement the `Monad` instance for `Logger`.
  // The `bind` implementation must append log values to maintain associativity.
  //
  // >>> (\a -> Logger (listh [4,5]) (a+3)) =<< Logger (listh [1,2]) 3
  // Logger [1,2,4,5] 6
  implicit def loggerMonad[L]: Monad[Logger[L, ?]] = ???

  // | A utility function for producing a `Logger` with one log value.
  //
  // >>> log1 1 2
  // Logger [1] 2
  def log1[L, A](log: L, a: A): Logger[L, A] = ???

  // Remove all duplicate integers from a list. Produce a log as you go.
  // If there is an element above 100, then abort the entire computation and produce no result.
  // However, always keep a log. If you abort the computation, produce a log with the value,
  // "aborting > 100: " followed by the value that caused it.
  // If you see an even number, produce a log message, "even number: " followed by the even number.
  // Other numbers produce no log message.
  //
  // /Tip:/ Use `filtering` and `StateT` over (`OptionalT` over `Logger` with a @Data.Set#Set@).
  //
  // >>> distinctG $ listh [1,2,3,2,6]
  // Logger ["even number: 2","even number: 2","even number: 6"] (Full [1,2,3,6])
  //
  // >>> distinctG $ listh [1,2,3,2,6,106]
  // Logger ["even number: 2","even number: 2","even number: 6","aborting > 100: 106"] Empty
  def distinctG(list: List[Int]): Logger[String, Optional[List[Int]]] = ???





  import org.scalactic.TripleEquals._

  implicit def loggerEq[A: Equality, B: Equality]: Equality[Logger[A, B]] = new Equality[Logger[A, B]] {
    def areEqual(logger: Logger[A, B], other: Any): Boolean = other match {
      case Logger(list, a) =>
        logger.list === list && logger.a === a
      case _ => false
    }
  }
}