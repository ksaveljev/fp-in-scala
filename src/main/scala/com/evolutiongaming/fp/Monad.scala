package com.evolutiongaming.fp

// All instances of the `Monad` type-class must satisfy one law. This law
// is not checked by the compiler. This law is given as:
//
// * The law of associativity
//   `∀f g x. g =<< (f =<< x) ≅ ((g =<<) . f) =<< x`
trait Monad[F[_]] extends Any with Applicative[F] {
  def flatMap[A, B](f: A => F[B])(fa: F[A]): F[B]

  // Witness that all things with (=<<) and (<$>) also have (<*>).
  //
  // >>> ExactlyOne (+10) <**> ExactlyOne 8
  // ExactlyOne 18
  //
  // >>> (+1) :. (*2) :. Nil <**> 1 :. 2 :. 3 :. Nil
  // [2,3,4,2,4,6]
  //
  // >>> Full (+8) <**> Full 7
  // Full 15
  //
  // >>> Empty <**> Full 7
  // Empty
  //
  // >>> Full (+8) <**> Empty
  // Empty
  //
  // >>> ((+) <**> (+10)) 3
  // 16
  //
  // >>> ((+) <**> (+5)) 3
  // 11
  //
  // >>> ((+) <**> (+5)) 1
  // 7
  //
  // >>> ((*) <**> (+10)) 3
  // 39
  //
  // >>> ((*) <**> (+2)) 3
  // 15
  def apply[A, B](f: F[A => B])(fa: F[A]): F[B] = ???
}

object Monad {
  import com.evolutiongaming.fp.ExactlyOne._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  // Binds a function on the ExactlyOne monad.
  //
  // >>> (\x -> ExactlyOne(x+1)) =<< ExactlyOne 2
  // ExactlyOne 3
  implicit val exactlyOneMonad: Monad[ExactlyOne] = ???

  // Binds a function on a List.
  //
  // >>> (\n -> n :. n :. Nil) =<< (1 :. 2 :. 3 :. Nil)
  // [1,1,2,2,3,3]
  implicit val listMonad: Monad[List] = ???

  // Binds a function on an Optional.
  //
  // >>> (\n -> Full (n + n)) =<< Full 7
  // Full 14
  implicit val optionalMonad: Monad[Optional] = ???

  // Binds a function on the reader ((->) t).
  //
  // >>> ((*) =<< (+10)) 7
  // 119
  implicit def functionMonad[T]: Monad[T => ?] = ???

  // Flattens a combined structure to a single structure.
  //
  // >>> join ((1 :. 2 :. 3 :. Nil) :. (1 :. 2 :. Nil) :. Nil)
  // [1,2,3,1,2]
  //
  // >>> join (Full Empty)
  // Empty
  //
  // >>> join (Full (Full 7))
  // Full 7
  //
  // >>> join (+) 7
  // 14
  def join[A, F[_]](fa: F[F[A]])(implicit F: Monad[F]): F[A] = ???

  // Implement a flipped version of @(=<<)@, however, use only
  // @join@ and @(<$>)@.
  // Pronounced, bind flipped.
  //
  // >>> ((+10) >>= (*)) 7
  // 119
  def bindFlipped[A, B, F[_]](f: A => F[B])(fa: F[A])(implicit F: Monad[F]): F[B] = ???

  // Implement composition within the @Monad@ environment.
  // Pronounced, kleisli composition.
  //
  // >>> ((\n -> n :. n :. Nil) <=< (\n -> n+1 :. n+2 :. Nil)) 1
  // [2,2,3,3]
  def kleisliArrow[A, B, C, F[_]](fab: A => F[B])(fbc: B => F[C])(implicit F: Monad[F]): A => F[C] = ???
}
