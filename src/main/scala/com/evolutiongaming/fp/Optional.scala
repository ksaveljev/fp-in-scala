package com.evolutiongaming.fp

import org.scalactic.Equality

sealed abstract class Optional[+A]

object Optional {

  // The `Optional` data type contains 0 or 1 value.
  //
  // It might be thought of as a list, with a maximum length of one.
  case class  Full[+A](a: A) extends Optional[A]
  case object Empty          extends Optional[Nothing]

  // Map the given function on the possible value.
  //
  // >>> mapOptional (+1) Empty
  // Empty
  //
  // >>> mapOptional (+1) (Full 8)
  // Full 9
  def mapOptional[A, B](f: A => B, opta: Optional[A]): Optional[B] = ???

  // Bind the given function on the possible value.
  //
  // >>> bindOptional Full Empty
  // Empty
  //
  // >>> bindOptional (\n -> if even n then Full (n - 1) else Full (n + 1)) (Full 8)
  // Full 7
  //
  // >>> bindOptional (\n -> if even n then Full (n - 1) else Full (n + 1)) (Full 9)
  // Full 10
  def bindOptional[A, B](f: A => Optional[B], opta: Optional[A]): Optional[B] = ???

  // Return the possible value if it exists; otherwise, the second argument.
  //
  // >>> Full 8 ?? 99
  // 8
  //
  // >>> Empty ?? 99
  // 99
  def ??[A](opta: Optional[A], aa: A): A = ???

  // Try the first optional for a value. If it has a value, use it; otherwise,
  // use the second value.
  //
  // >>> Full 8 <+> Empty
  // Full 8
  //
  // >>> Full 8 <+> Full 9
  // Full 8
  //
  // >>> Empty <+> Full 9
  // Full 9
  //
  // >>> Empty <+> Empty
  // Empty
  def <+>[A](opta1: Optional[A], opta2: Optional[A]): Optional[A] = ???



  def contains[A](a: A, opta: Optional[A]): Boolean =
    opta match {
      case Empty     => false
      case (Full(z)) => a == z
    }


  implicit class Or[A](val opta: Optional[A]) extends AnyVal {
    def ??(that: A): A = Optional.??(opta, that)
  }
  implicit class Orr[A](val opta: Optional[A]) extends AnyVal {
    def <+>(that: Optional[A]): Optional[A] = Optional.<+>(opta, that)
  }

  import org.scalactic.TripleEquals._

  implicit def optionalEq[A: Equality]: Equality[Optional[A]] = new Equality[Optional[A]] {
    def areEqual(a: Optional[A], other: Any): Boolean = other match {
      case b: Optional[_] =>
        (a, b) match {
          case (Empty, Empty)     => true
          case (Full(x), Full(y)) => x === y
          case _                  => false
        }
      case _ => false
    }
  }
}
