package com.evolutiongaming.fp

trait Functor[F[_]] extends Any {
  def fmap[A, B](f: A => B)(fa: F[A]): F[B]

  // Anonymous map. Maps a constant value on a functor.
  //
  // >>> 7 <$ (1 :. 2 :. 3 :. Nil)
  // [7,7,7]
  //
  // prop> x <$ (a :. b :. c :. Nil) == (x :. x :. x :. Nil)
  //
  // prop> x <$ Full q == Full x
  def amap[A, B](a: A, fb: F[B]): F[A] = ???

  // Anonymous map producing unit value.
  //
  // >>> void (1 :. 2 :. 3 :. Nil)
  // [(),(),()]
  //
  // >>> void (Full 7)
  // Full ()
  //
  // >>> void Empty
  // Empty
  //
  // >>> void (+10) 5
  // ()
  def void[A](fa: F[A]): F[Unit] = ???
}

object Functor {

  // Maps a function on the ExactlyOne functor.
  //
  // >>> (+1) <$> ExactlyOne 2
  // ExactlyOne 3
  implicit val exactlyOneFunctor: Functor[ExactlyOne] = ???

  // Maps a function on the List functor.
  //
  // >>> (+1) <$> Nil
  // []
  //
  // >>> (+1) <$> (1 :. 2 :. 3 :. Nil)
  // [2,3,4]
  implicit val listFunctor: Functor[List] = ???

  // Maps a function on the Optional functor.
  //
  // >>> (+1) <$> Empty
  // Empty
  //
  // >>> (+1) <$> Full 2
  // Full 3
  implicit val optionalFunctor: Functor[Optional] = ???

  // Maps a function on the reader ((->) t) functor.
  //
  // >>> ((+1) <$> (*2)) 8
  // 17
  implicit def functionFunctor[T]: Functor[T => ?] = ???
}