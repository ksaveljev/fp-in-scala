package com.evolutiongaming.fp

import com.evolutiongaming.fp.Optional._
import org.scalactic.Equality

import scala.annotation.tailrec
import scala.collection.immutable.{List => ScalaList}

sealed abstract class List[+A] {
  def isEmpty: Boolean
  def head: A
  def tail: List[A]
}

object List {

  final class ::[+A](h: A, t: => List[A]) extends List[A] {
    def isEmpty = false
    def head: A = h
    def tail: List[A] = t
    override def toString = s"$h :: ${t.toString}"
  }
  object Nil extends List[Nothing] {
    def isEmpty = true
    def head = throw new NoSuchElementException("head of empty list")
    def tail = throw new UnsupportedOperationException("tail of empty list")
    override def toString = "Nil"
  }

  // The list of integers from zero to infinity.
  def infinity: List[Int] = {
    def inf(x: Int): List[Int] = x :: inf(x + 1)
    inf(0)
  }

  // functions over List that you may consider using
  def foldRight[A, B](f: (A, => B) => B)(b: B)(list: List[A]): B =
    list match {
      case Nil      => b
      case (h :: t) => f(h, foldRight(f)(b)(t))
    }

  @tailrec
  def foldLeft[A, B](f: (B, A) => B, b: B, list: List[A]): B =
    list match {
      case Nil      => b
      case (h :: t) => foldLeft(f, f(b, h), t)
    }


  // Returns the head of the list or the given default.
  //
  // >>> headOr 3 (1 :. 2 :. Nil)
  // 1
  //
  // >>> headOr 3 Nil
  // 3
  //
  // prop> x `headOr` infinity == 0
  //
  // prop> x `headOr` Nil == x
  def headOr[A](a: A)(list: List[A]): A = ???

  // The product of the elements of a list.
  //
  // >>> product Nil
  // 1
  //
  // >>> product (1 :. 2 :. 3 :. Nil)
  // 6
  //
  // >>> product (1 :. 2 :. 3 :. 4 :. Nil)
  // 24
  def product(list: List[Int]): Int = ???

  // Sum the elements of the list.
  //
  // >>> sum (1 :. 2 :. 3 :. Nil)
  // 6
  //
  // >>> sum (1 :. 2 :. 3 :. 4 :. Nil)
  // 10
  //
  // prop> foldLeft (-) (sum x) x == 0
  def sum(list: List[Int]): Int = ???

  // | Return the length of the list.
  //
  // >>> length (1 :. 2 :. 3 :. Nil)
  // 3
  //
  // prop> sum (map (const 1) x) == length x
  def length[A](list: List[A]): Int = ???

  // Map the given function on each element of the list.
  //
  // >>> map (+10) (1 :. 2 :. 3 :. Nil)
  // [11,12,13]
  //
  // prop> headOr x (map (+1) infinity) == 1
  //
  // prop> map id x == x
  def map[A, B](f: A => B)(list: List[A]): List[B] = ???

  // Return elements satisfying the given predicate.
  //
  // >>> filter even (1 :. 2 :. 3 :. 4 :. 5 :. Nil)
  // [2,4]
  //
  // prop> headOr x (filter (const True) infinity) == 0
  //
  // prop> filter (const True) x == x
  //
  // prop> filter (const False) x == Nil
  def filter[A](p: A => Boolean)(list: List[A]): List[A] = ???

  // Append two lists to a new list.
  //
  // >>> (1 :. 2 :. 3 :. Nil) ++ (4 :. 5 :. 6 :. Nil)
  // [1,2,3,4,5,6]
  //
  // prop> headOr x (Nil ++ infinity) == 0
  //
  // prop> headOr x (y ++ infinity) == headOr 0 y
  //
  // prop> (x ++ y) ++ z == x ++ (y ++ z)
  //
  // prop> x ++ Nil == x
  def ++:[A](list1: List[A], list2: List[A]): List[A] = ???

  // Flatten a list of lists to a list.
  //
  // >>> flatten ((1 :. 2 :. 3 :. Nil) :. (4 :. 5 :. 6 :. Nil) :. (7 :. 8 :. 9 :. Nil) :. Nil)
  // [1,2,3,4,5,6,7,8,9]
  //
  // prop> headOr x (flatten (infinity :. y :. Nil)) == 0
  //
  // prop> headOr x (flatten (y :. infinity :. Nil)) == headOr 0 y
  //
  // prop> sum (map length x) == length (flatten x)
  def flatten[A](list: List[List[A]]): List[A] = ???

  // Map a function then flatten to a list.
  //
  // >>> flatMap (\x -> x :. x + 1 :. x + 2 :. Nil) (1 :. 2 :. 3 :. Nil)
  // [1,2,3,2,3,4,3,4,5]
  //
  // prop> headOr x (flatMap id (infinity :. y :. Nil)) == 0
  //
  // prop> headOr x (flatMap id (y :. infinity :. Nil)) == headOr 0 y
  //
  // prop> flatMap id (x :: List (List Int)) == flatten x
  def flatMap[A, B](f: A => List[B])(list: List[A]): List[B] = ???

  // Flatten a list of lists to a list (again).
  // HOWEVER, this time use the /flatMap/ function that you just wrote.
  //
  // prop> let types = x :: List (List Int) in flatten x == flattenAgain x
  def flattenAgain[A](list: List[List[A]]): List[A] = ???

  // Convert a list of optional values to an optional list of values.
  //
  // * If the list contains all `Full` values,
  // then return `Full` list of values.
  //
  // * If the list contains one or more `Empty` values,
  // then return `Empty`.
  //
  // * The only time `Empty` is returned is
  // when the list contains one or more `Empty` values.
  //
  // >>> seqOptional (Full 1 :. Full 10 :. Nil)
  // Full [1,10]
  //
  // >>> seqOptional Nil
  // Full []
  //
  // >>> seqOptional (Full 1 :. Full 10 :. Empty :. Nil)
  // Empty
  //
  // >>> seqOptional (Empty :. map Full infinity)
  // Empty
  def seqOptional[A](list: List[Optional[A]]): Optional[List[A]] = ???

  // Find the first element in the list matching the predicate.
  //
  // >>> find even (1 :. 3 :. 5 :. Nil)
  // Empty
  //
  // >>> find even Nil
  // Empty
  //
  // >>> find even (1 :. 2 :. 3 :. 5 :. Nil)
  // Full 2
  //
  // >>> find even (1 :. 2 :. 3 :. 4 :. 5 :. Nil)
  // Full 2
  //
  // >>> find (const True) infinity
  // Full 0
  def find[A](p: A => Boolean)(list: List[A]): Optional[A] = ???

  // Determine if the length of the given list is greater than 4.
  //
  // >>> lengthGT4 (1 :. 3 :. 5 :. Nil)
  // False
  //
  // >>> lengthGT4 Nil
  // False
  //
  // >>> lengthGT4 (1 :. 2 :. 3 :. 4 :. 5 :. Nil)
  // True
  //
  // >>> lengthGT4 infinity
  // True
  def lengthGT4[A](list: List[A]): Boolean = ???

  // Reverse a list.
  //
  // >>> reverse Nil
  // []
  //
  // >>> take 1 (reverse (reverse largeList))
  // [1]
  //
  // prop> let types = x :: List Int in reverse x ++ reverse y == reverse (y ++ x)
  //
  // prop> let types = x :: Int in reverse (x :. Nil) == x :. Nil
  def reverse[A](list: List[A]): List[A] = ???

  // Produce an infinite `List` that seeds with the given value at its head,
  // then runs the given function for subsequent elements
  //
  // >>> let (x:.y:.z:.w:._) = produce (+1) 0 in [x,y,z,w]
  // [0,1,2,3]
  //
  // >>> let (x:.y:.z:.w:._) = produce (*2) 1 in [x,y,z,w]
  // [1,2,4,8]
  def produce[A](f: A => A)(a: A): List[A] =
    a :: produce(f)(f(a))







  def take[A](n: Int, list: List[A]): List[A] =
    if (n <= 0) Nil else list match {
      case Nil       => Nil
      case (x :: xs) => x :: take(n-1, xs)
    }

  def repeat[A](a: A): List[A] =
    a :: repeat(a)

  def replicate[A](n: Int, a: A): List[A] =
    take(n, repeat(a))

  def dropWhile[A](p: A => Boolean)(list: List[A]): List[A] =
    list match {
      case Nil    => Nil
      case h :: t => if (p(h)) dropWhile(p)(t) else list
    }

  def takeWhile[A](list: List[A], p: A => Boolean): List[A] =
    list match {
      case Nil    => Nil
      case h :: t => if (p(h)) h :: takeWhile(t, p) else Nil
    }

  def span[A](list: List[A], p: A => Boolean): (List[A], List[A]) =
    (takeWhile(list, p), dropWhile(p)(list))

  def break[A](p: A => Boolean)(list: List[A]): (List[A], List[A]) =
    span(list, (a: A) => !p(a))

  def zipWith[A, B, C](f: A => B => C)(a: List[A])(b: List[B]): List[C] =
    (a, b) match {
      case (ah :: at, bh :: bt) => f(ah)(bh) :: zipWith(f)(at)(bt)
      case _                    => Nil
    }

  def unfoldr[A, B](f: A => Optional[(B, A)])(a: A): List[B] =
    f(a) match {
      case Empty         => Nil
      case Full((b, aa)) => b :: unfoldr(f)(aa)
    }

  def all[A](p: A => Boolean)(list: List[A]): Boolean = {
    def combine(a: A, b: => Boolean) = p(a) && b
    foldRight(combine)(true)(list)
  }






  def scalaList[A](list: List[A]): ScalaList[A] = {
    def combine(a: A, b: => ScalaList[A]) = a +: b
    foldRight(combine)(ScalaList.empty)(list)
  }

  def listScala[A](list: ScalaList[A]): List[A] =
    list.foldRight(Nil: List[A])((a: A, b: List[A]) => a :: b)

  def largeList: List[Int] =
    listScala(ScalaList.range(1, 50000))







  class ConsWrapper[A](t: => List[A]) {
    def ::[B >: A](h: B): List[B] = cons(h, t)
  }

  implicit def consWrapper[A](list: => List[A]): ConsWrapper[A] =
    new ConsWrapper[A](list)

  object :: {
    def unapply[A](xs: List[A]): Option[(A, List[A])] =
      if (xs.isEmpty) None
      else Some((xs.head, xs.tail))
  }

  object cons {
    def apply[A](h: A, t: => List[A]) = new List.::(h, t)
    def unapply[A](xs: List[A]): Option[(A, List[A])] = ::.unapply(xs)
  }

  implicit class Append[A](val list: List[A]) extends AnyVal {
    def ++:[B >: A](that: List[B]): List[B] = List.++:(that, list)
  }

  import org.scalactic.TripleEquals._

  implicit def listEq[A: Equality]: Equality[List[A]] = new Equality[List[A]] {

    def areEqual(a: List[A], other: Any): Boolean = other match {
      case b: List[_] =>
        if (length(a) == length(b))
          a match {
            case Nil => true
            case _   => a.head === b.head && a.tail === b.tail
          }
        else
          false

      case _ => false
    }
  }
}


