package com.evolutiongaming.fp

// Exactly one of these exercises will not be possible to achieve. Determine which.

trait Compose[F[_], G[_], A] {
  val v: F[G[A]]
}

object Compose {

  import com.evolutiongaming.fp.Applicative._

  // Implement a Functor instance for Compose
  implicit def composeFunctor[F[_], G[_]](implicit F: Functor[F], G: Functor[G]): Functor[Compose[F, G, ?]] = ???

  // Implement an Applicative instance for Compose
  implicit def composeApplicative[F[_], G[_]](implicit F: Applicative[F], G: Applicative[G]): Applicative[Compose[F, G, ?]] = ???

  implicit def composeMonad[F[_], G[_]](implicit F: Monad[F], G: Monad[G]): Monad[Compose[F, G, ?]] = ???

}
