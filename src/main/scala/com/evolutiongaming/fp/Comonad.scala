package com.evolutiongaming.fp

// All instances of the `Comonad` type-class must satisfy two laws. These
// laws are not checked by the compiler. These laws are given as:
//
// * The law of left identity
//   `∀x. copure <<= x ≅ x`
//
// * The law of right identity
//   `∀f. copure . (f <<=) == f
trait Comonad[F[_]] {
  val extendF: Extend[F]
  def copure[A](fa: F[A]): A
}

object Comonad {

  // Implement the @Comonad@ instance for @ExactlyOne@.
  //
  // >>> copure (ExactlyOne 7)
  // 7
  implicit val exactlyOneComonad: Comonad[ExactlyOne] = ???

  // Witness that all things with (<<=) and copure also have (<$>).
  //
  // >>> (+10) <$> ExactlyOne 7
  // ExactlyOne 17
  def fmap[A, B, F[_]](fa: F[A])(f: A => B)(implicit F: Comonad[F]): F[B] = ???

}
