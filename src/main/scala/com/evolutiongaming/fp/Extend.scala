package com.evolutiongaming.fp

// | All instances of the `Extend` type-class must satisfy one law. This law
// is not checked by the compiler. This law is given as:
//
// * The law of associativity
//   `∀f g. (f <<=) . (g <<=) ≅ (<<=) (f . (g <<=))`
trait Extend[F[_]] {
  def extend[A, B](f: F[A] => B)(fa: F[A]): F[B]
}

object Extend {
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  // | Implement the @Extend@ instance for @ExactlyOne@.
  //
  // >>> id <<= ExactlyOne 7
  // ExactlyOne (ExactlyOne 7)
  implicit val exactlyOneExtend: Extend[ExactlyOne] = ???

  // Implement the @Extend@ instance for @List@.
  //
  // >>> length <<= ('a' :. 'b' :. 'c' :. Nil)
  // [3,2,1]
  //
  // >>> id <<= (1 :. 2 :. 3 :. 4 :. Nil)
  // [[1,2,3,4],[2,3,4],[3,4],[4]]
  //
  // >>> reverse <<= ((1 :. 2 :. 3 :. Nil) :. (4 :. 5 :. 6 :. Nil) :. Nil)
  // [[[4,5,6],[1,2,3]],[[4,5,6]]]
  implicit val listExtend: Extend[List] = ???

  // Implement the @Extend@ instance for @Optional@.
  //
  // >>> id <<= (Full 7)
  // Full (Full 7)
  //
  // >>> id <<= Empty
  // Empty
  implicit val optionalExtend: Extend[Optional] = ???

  // Duplicate the functor using extension.
  //
  // >>> cojoin (ExactlyOne 7)
  // ExactlyOne (ExactlyOne 7)
  //
  // >>> cojoin (1 :. 2 :. 3 :. 4 :. Nil)
  // [[1,2,3,4],[2,3,4],[3,4],[4]]
  //
  // >>> cojoin (Full 7)
  // Full (Full 7)
  //
  // >>> cojoin Empty
  // Empty
  def cojoin[A, F[_]](fa: F[A])(implicit F: Extend[F]): F[F[A]] = ???

}
