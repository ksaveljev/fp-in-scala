package com.evolutiongaming.fp

// All instances of the `Applicative` type-class must satisfy three laws.
// These laws are not checked by the compiler. These laws are given as:
//
// * The law of associative composition
//   `∀a b c. ((.) <$> a <*> b <*> c) ≅ (a <*> (b <*> c))`
//
// * The law of left identity
//   `∀x. pure id <*> x ≅ x`
//
// * The law of right identity
//   `∀x. x <*> pure id ≅ x`
trait Applicative[F[_]] extends Any with Functor[F] {

  def apply[A, B](f: F[A => B])(fa: F[A]): F[B]
  def pure[A](a: A): F[A]

  // Witness that all things with (<*>) and pure also have (<$>).
  //
  // >>> (+1) <$$> (ExactlyOne 2)
  // ExactlyOne 3
  //
  // >>> (+1) <$$> Nil
  // []
  //
  // >>> (+1) <$$> (1 :. 2 :. 3 :. Nil)
  // [2,3,4]
  def fmap[A, B](f: A => B)(fa: F[A]): F[B] = ???

  // Apply a binary function in the environment.
  //
  // >>> lift2 (+) (ExactlyOne 7) (ExactlyOne 8)
  // ExactlyOne 15
  //
  // >>> lift2 (+) (1 :. 2 :. 3 :. Nil) (4 :. 5 :. Nil)
  // [5,6,6,7,7,8]
  //
  // >>> lift2 (+) (Full 7) (Full 8)
  // Full 15
  //
  // >>> lift2 (+) (Full 7) Empty
  // Empty
  //
  // >>> lift2 (+) Empty (Full 8)
  // Empty
  //
  // >>> lift2 (+) length sum (listh [4,5,6])
  // 18
  def lift2[A, B, C](f: A => B => C)(fa: F[A])(fb: F[B]): F[C] = ???

  // Apply a ternary function in the environment.
  //
  // >>> lift3 (\a b c -> a + b + c) (ExactlyOne 7) (ExactlyOne 8) (ExactlyOne 9)
  // ExactlyOne 24
  //
  // >>> lift3 (\a b c -> a + b + c) (1 :. 2 :. 3 :. Nil) (4 :. 5 :. Nil) (6 :. 7 :. 8 :. Nil)
  // [11,12,13,12,13,14,12,13,14,13,14,15,13,14,15,14,15,16]
  //
  // >>> lift3 (\a b c -> a + b + c) (Full 7) (Full 8) (Full 9)
  // Full 24
  //
  // >>> lift3 (\a b c -> a + b + c) (Full 7) (Full 8) Empty
  // Empty
  //
  // >>> lift3 (\a b c -> a + b + c) Empty (Full 8) (Full 9)
  // Empty
  //
  // >>> lift3 (\a b c -> a + b + c) Empty Empty (Full 9)
  // Empty
  //
  // >>> lift3 (\a b c -> a + b + c) length sum product (listh [4,5,6])
  // 138
  def lift3[A, B, C, D](f: A => B => C => D)(fa: F[A])(fb: F[B])(fc: F[C]): F[D] = ???

  // Apply a quaternary function in the environment.
  //
  // >>> lift4 (\a b c d -> a + b + c + d) (ExactlyOne 7) (ExactlyOne 8) (ExactlyOne 9) (ExactlyOne 10)
  // ExactlyOne 34
  //
  // >>> lift4 (\a b c d -> a + b + c + d) (1 :. 2 :. 3 :. Nil) (4 :. 5 :. Nil) (6 :. 7 :. 8 :. Nil) (9 :. 10 :. Nil)
  // [20,21,21,22,22,23,21,22,22,23,23,24,21,22,22,23,23,24,22,23,23,24,24,25,22,23,23,24,24,25,23,24,24,25,25,26]
  //
  // >>> lift4 (\a b c d -> a + b + c + d) (Full 7) (Full 8) (Full 9) (Full 10)
  // Full 34
  //
  // >>> lift4 (\a b c d -> a + b + c + d) (Full 7) (Full 8) Empty  (Full 10)
  // Empty
  //
  // >>> lift4 (\a b c d -> a + b + c + d) Empty (Full 8) (Full 9) (Full 10)
  // Empty
  //
  // >>> lift4 (\a b c d -> a + b + c + d) Empty Empty (Full 9) (Full 10)
  // Empty
  //
  // >>> lift4 (\a b c d -> a + b + c + d) length sum product (sum . filter even) (listh [4,5,6])
  // 148
  def lift4[A, B, C, D, E](f: A => B => C => D => E)(fa: F[A])(fb: F[B])(fc: F[C])(fd: F[D]): F[E] = ???

  // Apply, discarding the value of the first argument.
  // Pronounced, right apply.
  //
  // >>> (1 :. 2 :. 3 :. Nil) *> (4 :. 5 :. 6 :. Nil)
  // [4,5,6,4,5,6,4,5,6]
  //
  // >>> (1 :. 2 :. Nil) *> (4 :. 5 :. 6 :. Nil)
  // [4,5,6,4,5,6]
  //
  // >>> (1 :. 2 :. 3 :. Nil) *> (4 :. 5 :. Nil)
  // [4,5,4,5,4,5]
  //
  // >>> Full 7 *> Full 8
  // Full 8
  //
  // prop> (a :. b :. c :. Nil) *> (x :. y :. z :. Nil) == (x :. y :. z :. x :. y :. z :. x :. y :. z :. Nil)
  //
  // prop> Full x *> Full y == Full y
  def rapply[A, B](fa: F[A])(fb: F[B]): F[B] = ???

  // Apply, discarding the value of the second argument.
  // Pronounced, left apply.
  //
  // >>> (1 :. 2 :. 3 :. Nil) <* (4 :. 5 :. 6 :. Nil)
  // [1,1,1,2,2,2,3,3,3]
  //
  // >>> (1 :. 2 :. Nil) <* (4 :. 5 :. 6 :. Nil)
  // [1,1,1,2,2,2]
  //
  // >>> (1 :. 2 :. 3 :. Nil) <* (4 :. 5 :. Nil)
  // [1,1,2,2,3,3]
  //
  // >>> Full 7 <* Full 8
  // Full 7
  //
  // prop> (x :. y :. z :. Nil) <* (a :. b :. c :. Nil) == (x :. x :. x :. y :. y :. y :. z :. z :. z :. Nil)
  //
  // prop> Full x <* Full y == Full x
  def lapply[A, B](fb: F[B])(fa: F[A]): F[B] = ???
}

object Applicative {
  import com.evolutiongaming.fp.ExactlyOne._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  // Insert into ExactlyOne.
  //
  // prop> pure x == ExactlyOne x
  //
  // >>> ExactlyOne (+10) <*> ExactlyOne 8
  // ExactlyOne 18
  implicit val exactlyOneApplicative: Applicative[ExactlyOne] = ???

  // Insert into a List.
  //
  // prop> pure x == x :. Nil
  //
  // >>> (+1) :. (*2) :. Nil <*> 1 :. 2 :. 3 :. Nil
  // [2,3,4,2,4,6]
  implicit val listApplicative: Applicative[List] = ???

  // Insert into an Optional.
  //
  // prop> pure x == Full x
  //
  // >>> Full (+8) <*> Full 7
  // Full 15
  //
  // >>> Empty <*> Full 7
  // Empty
  //
  // >>> Full (+8) <*> Empty
  // Empty
  implicit val optionalApplicative: Applicative[Optional] = ???

  // Insert into a constant function.
  //
  // >>> ((+) <*> (+10)) 3
  // 16
  //
  // >>> ((+) <*> (+5)) 3
  // 11
  //
  // >>> ((+) <*> (+5)) 1
  // 7
  //
  // >>> ((*) <*> (+10)) 3
  // 39
  //
  // >>> ((*) <*> (+2)) 3
  // 15
  //
  // prop> pure x y == x
  implicit def functionApplicative[T]: Applicative[T => ?] = ???

  // Sequences a list of structures to a structure of list.
  //
  // >>> sequence (ExactlyOne 7 :. ExactlyOne 8 :. ExactlyOne 9 :. Nil)
  // ExactlyOne [7,8,9]
  //
  // >>> sequence ((1 :. 2 :. 3 :. Nil) :. (1 :. 2 :. Nil) :. Nil)
  // [[1,1],[1,2],[2,1],[2,2],[3,1],[3,2]]
  //
  // >>> sequence (Full 7 :. Empty :. Nil)
  // Empty
  //--
  // >>> sequence (Full 7 :. Full 8 :. Nil)
  // Full [7,8]
  //
  // >>> sequence ((*10) :. (+2) :. Nil) 6
  // [60,8]
  def sequence[A, F[_]](list: List[F[A]])(implicit F: Applicative[F]): F[List[A]] = ???

  // Replicate an effect a given number of times.
  //
  // >>> replicateA 4 (ExactlyOne "hi")
  // ExactlyOne ["hi","hi","hi","hi"]
  //
  // >>> replicateA 4 (Full "hi")
  // Full ["hi","hi","hi","hi"]
  //
  // >>> replicateA 4 Empty
  // Empty
  //
  // >>> replicateA 4 (*2) 5
  // [10,10,10,10]
  //
  // >>> replicateA 3 ('a' :. 'b' :. 'c' :. Nil)
  // ["aaa","aab","aac","aba","abb","abc","aca","acb","acc","baa","bab","bac","bba","bbb","bbc","bca","bcb","bcc","caa","cab","cac","cba","cbb","cbc","cca","ccb","ccc"]
  def replicateA[A, F[_]](n: Int, fa: F[A])(implicit F: Applicative[F]): F[List[A]] = ???

  // Filter a list with a predicate that produces an effect.
  //
  // >>> filtering (ExactlyOne . even) (4 :. 5 :. 6 :. Nil)
  // ExactlyOne [4,6]
  //
  // >>> filtering (\a -> if a > 13 then Empty else Full (a <= 7)) (4 :. 5 :. 6 :. Nil)
  // Full [4,5,6]
  //
  // >>> filtering (\a -> if a > 13 then Empty else Full (a <= 7)) (4 :. 5 :. 6 :. 7 :. 8 :. 9 :. Nil)
  // Full [4,5,6,7]
  //
  // >>> filtering (\a -> if a > 13 then Empty else Full (a <= 7)) (4 :. 5 :. 6 :. 13 :. 14 :. Nil)
  // Empty
  //
  // >>> filtering (>) (4 :. 5 :. 6 :. 7 :. 8 :. 9 :. 10 :. 11 :. 12 :. Nil) 8
  // [9,10,11,12]
  //
  // >>> filtering (const $ True :. True :.  Nil) (1 :. 2 :. 3 :. Nil)
  // [[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3],[1,2,3]]
  //
  def filtering[A, F[_]](p: A => F[Boolean], list: List[A])(implicit F: Applicative[F]): F[List[A]] = ???

}
