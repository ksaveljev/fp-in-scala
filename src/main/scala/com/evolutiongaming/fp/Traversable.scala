package com.evolutiongaming.fp

// All instances of the `Traversable` type-class must satisfy two laws. These
// laws are not checked by the compiler. These laws are given as:
//
// * The law of naturality
//   `∀f g. f . traverse g ≅ traverse (f . g)`
//
// * The law of identity
//   `∀x. traverse ExactlyOne x ≅ ExactlyOne x`
//
// * The law of composition
//   `∀f g. traverse ((g <$>) . f) ≅ (traverse g <$>) . traverse f`
trait Traversable[T[_]] {
  val traversableF: Functor[T]
  def traverse[A, B, F[+_]: Applicative](f: A => F[B])(ta: T[A]): F[T[B]]
}

object Traversable {

  import com.evolutiongaming.fp.List._

  implicit def listTraversable: Traversable[List] = new Traversable[List] {
    val traversableF: Functor[List] = implicitly
    def traverse[A, B, F[_]](f: A => F[B])(list: List[A])(implicit F: Applicative[F]): F[List[B]] = {
      def combine(a: A, b: => F[List[B]]): F[List[B]] = {
        F.apply(F.fmap((r: B) => (s: List[B]) => r :: s)(f(a)))(b)
      }
      foldRight(combine)(F.pure(Nil: List[B]))(list)
    }
  }

}
