package com.evolutiongaming.fp

// A `State` is a function from a state value `s` to (a produced value `a`, and a resulting state `s`).
trait State[S, A] {
  def run(s: S): (A, S)
}

object State {

  import com.evolutiongaming.fp.Applicative.filtering
  import com.evolutiongaming.fp.Functor._
  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Monad._
  import com.evolutiongaming.fp.Optional._

  // Run the `State` seeded with `s` and retrieve the resulting state.
  //
  // prop> \(Fun _ f) -> exec (State f) s == snd (runState (State f) s)
  def exec[S, A](k: State[S, A], s: S): S = ???

  // Run the `State` seeded with `s` and retrieve the resulting value.
  //
  // prop> \(Fun _ f) -> eval (State f) s == fst (runState (State f) s)
  def eval[S, A](k: State[S, A], s: S): A = ???

  // A `State` where the state also distributes into the produced value.
  //
  // >>> runState get 0
  // (0,0)
  def get[S]: State[S, S] = ???

  // A `State` where the resulting state is seeded with the given value.
  //
  // >>> runState (put 1) 0
  // ((),1)
  def put[S](v: S): State[S, Unit] = ???

  // Implement the `Functor` instance for `State s`.
  //
  // >>> runState ((+1) <$> State (\s -> (9, s * 2))) 3
  // (10,6)
  implicit def stateFunctor[S]: Functor[State[S, ?]] = ???

  // Implement the `Applicative` instance for `State s`.
  //
  // >>> runState (pure 2) 0
  // (2,0)
  //
  // >>> runState (pure (+1) <*> pure 0) 0
  // (1,0)
  //
  // >>> import qualified Prelude as P
  // >>> runState (State (\s -> ((+3), s P.++ ["apple"])) <*> State (\s -> (7, s P.++ ["banana"]))) []
  // (10,["apple","banana"])
  implicit def stateApplicative[S]: Applicative[State[S, ?]] = ???

  // Implement the `Bind` instance for `State s`.
  //
  // >>> runState ((const $ put 2) =<< put 1) 0
  // ((),2)
  //
  // >>> let modify f = State (\s -> ((), f s)) in runState (modify (+1) >>= \() -> modify (*2)) 7
  // ((),16)
  implicit def stateMonad[S]: Monad[State[S, ?]] = ???

  // Find the first element in a `List` that satisfies a given predicate.
  // It is possible that no element is found, hence an `Optional` result.
  // However, while performing the search, we sequence some `Monad` effect through.
  //
  // Note the similarity of the type signature to List#find
  // where the effect appears in every return position:
  //   find ::  (a ->   Bool) -> List a ->    Optional a
  //   findM :: (a -> f Bool) -> List a -> f (Optional a)
  //
  // >>> let p x = (\s -> (const $ pure (x == 'c')) =<< put (1+s)) =<< get in runState (findM p $ listh ['a'..'h']) 0
  // (Full 'c',3)
  //
  // >>> let p x = (\s -> (const $ pure (x == 'i')) =<< put (1+s)) =<< get in runState (findM p $ listh ['a'..'h']) 0
  // (Empty,8)
  def findM[A, F[_]](p: A => F[Boolean])(list: List[A])(implicit F: Monad[F]): F[Optional[A]] = ???

  // Find the first element in a `List` that repeats.
  // It is possible that no element repeats, hence an `Optional` result.
  //
  // /Tip:/ Use `findM` and `State` with a @Data.Set#Set@.
  //
  // prop> case firstRepeat xs of Empty -> let xs' = hlist xs in nub xs' == xs'; Full x -> length (filter (== x) xs) > 1
  // prop> case firstRepeat xs of Empty -> True; Full x -> let (l, (rx :. rs)) = span (/= x) xs in let (l2, r2) = span (/= x) rs in let l3 = hlist (l ++ (rx :. Nil) ++ l2) in nub l3 == l3
  def firstRepeat[A](list: List[A]): Optional[A] = ???

  // | Remove all duplicate elements in a `List`.
  // /Tip:/ Use `filtering` and `State` with a @Data.Set#Set@.
  //
  // prop> firstRepeat (distinct xs) == Empty
  //
  // prop> distinct xs == distinct (flatMap (\x -> x :. x :. Nil) xs)
  def distinct[A](list: List[A]): List[A] = ???

  // A happy number is a positive integer, where the sum of the square of its digits eventually reaches 1 after repetition.
  // In contrast, a sad number (not a happy number) is where the sum of the square of its digits never reaches 1
  // because it results in a recurring sequence.
  //
  // /Tip:/ Use `firstRepeat` with `produce`.
  //
  // /Tip:/ Use `join` to write a @square@ function.
  //
  // /Tip:/ Use library functions: @Optional#contains@, @Data.Char#digitToInt@.
  //
  // >>> isHappy 4
  // False
  //
  // >>> isHappy 7
  // True
  //
  // >>> isHappy 42
  // False
  //
  // >>> isHappy 44
  // True
  def isHappy(n: Int): Boolean = ???
}