package com.evolutiongaming.fp

sealed trait Validation[+A]

object Validation {

  type Err = String

  case class Error(msg: Err) extends Validation[Nothing]
  case class Value[A](a: A)  extends Validation[A]

  // Returns whether or not the given validation is an error.
  //
  // >>> isError (Error "message")
  // True
  //
  // >>> isError (Value 7)
  // False
  //
  // prop> isError x /= isValue x
  def isError[A](v: Validation[A]): Boolean =
    v match {
      case Error(_) => true
      case Value(_) => false
    }

  // Returns whether or not the given validation is a value.
  //
  // >>> isValue (Error "message")
  // False
  //
  // >>> isValue (Value 7)
  // True
  //
  // prop> isValue x /= isError x
  def isValue[A](v: Validation[A]): Boolean =
    !isError(v)

  // Maps a function on a validation's value side.
  //
  // >>> mapValidation (+10) (Error "message")
  // Error "message"
  //
  // >>> mapValidation (+10) (Value 7)
  // Value 17
  //
  // prop> mapValidation id x == x
  def mapValidation[A, B](f: A => B)(v: Validation[A]): Validation[B] =
    v match {
      case Error(msg) => Error(msg)
      case Value(a)   => Value(f(a))
    }

  // Binds a function on a validation's value side to a new validation.
  //
  // >>> bindValidation (\n -> if even n then Value (n + 10) else Error "odd") (Error "message")
  // Error "message"
  //
  // >>> bindValidation (\n -> if even n then Value (n + 10) else Error "odd") (Value 7)
  // Error "odd"
  //
  // >>> bindValidation (\n -> if even n then Value (n + 10) else Error "odd") (Value 8)
  // Value 18
  //
  // prop> bindValidation Value x == x
  def bindValidation[A, B](f: A => Validation[B])(v: Validation[A]): Validation[B] =
    v match {
      case Error(msg) => Error(msg)
      case Value(a)   => f(a)
    }

  // Returns a validation's value side or the given default if it is an error.
  //
  // >>> valueOr (Error "message") 3
  // 3
  //
  // >>> valueOr (Value 7) 3
  // 7
  //
  // prop> isValue x || valueOr x n == n
  def valueOr[A](v: Validation[A])(a: A): A =
    v match {
      case Error(_)  => a
      case Value(aa) => aa
    }

  // Returns a validation's error side or the given default if it is a value.
  //
  // >>> errorOr (Error "message") "q"
  // "message"
  //
  // >>> errorOr (Value 7) "q"
  // "q"
  //
  // prop> isError x || errorOr x e == e
  def errorOr[A](v: Validation[A])(msg: Err): Err =
    v match {
      case Error(e) => e
      case Value(_) => msg
    }

  def valueValidation[A](a: A): Validation[A] =
    Value(a)
}

