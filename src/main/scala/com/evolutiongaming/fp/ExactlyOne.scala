package com.evolutiongaming.fp

import org.scalactic.Equality

case class ExactlyOne[A](a: A) extends AnyVal

object ExactlyOne {

  def runExactlyOne[A](e: ExactlyOne[A]): A =
    e.a

  def mapExactlyOne[A, B](e: ExactlyOne[A])(f: A => B): ExactlyOne[B] =
    ExactlyOne(f(e.a))

  def bindExactlyOne[A, B](e: ExactlyOne[A])(f: A => ExactlyOne[B]): ExactlyOne[B] =
    f(e.a)



  import org.scalactic.TripleEquals._

  implicit def exactlyOneEq[A: Equality]: Equality[ExactlyOne[A]] = new Equality[ExactlyOne[A]] {

    def areEqual(a: ExactlyOne[A], other: Any): Boolean = other match {
      case b: ExactlyOne[_] =>
        a.a === b.a

      case _ => false
    }
  }
}

