package com.evolutiongaming.fp

import org.scalactic.Equality

import scala.collection.immutable.{List => ScalaList}
import scala.annotation.tailrec

// A `ListZipper` is a focussed position, with a list of values to the left and to the right.
//
// For example, taking the list [0,1,2,3,4,5,6], the moving focus to the third position, the zipper looks like:
// ListZipper [2,1,0] 3 [4,5,6]
//
// Supposing then we move left on this zipper:
// ListZipper [1,0] 2 [3,4,5,6]
//
// then suppose we add 17 to the focus of this zipper:
// ListZipper [1,0] 19 [3,4,5,6]
case class ListZipper[+A](l: List[A], m: A, r: List[A])

object ListZipper {

  import com.evolutiongaming.fp.List._
  import com.evolutiongaming.fp.Optional._

  def lefts[A](z: ListZipper[A]): List[A] = z.l
  def rights[A](z: ListZipper[A]): List[A] = z.r

  // A `MaybeListZipper` is a data structure that allows us to "fail" zipper operations.
  // e.g. Moving left when there are no values to the left.
  //
  // We then overload operations polymorphically to operate on both `ListZipper` and `MaybeListZipper`
  // using the `ListZipper'` type-class below.
  sealed trait MaybeListZipper[+A]

  case class  IsZ[+A](z: ListZipper[A]) extends MaybeListZipper[A]
  case object IsNotZ                    extends MaybeListZipper[Nothing]

  // Implement the `Functor` instance for `ListZipper`.
  //
  // >>> (+1) <$> (zipper [3,2,1] 4 [5,6,7])
  // [4,3,2] >5< [6,7,8]
  implicit val listZipperFunctor: Functor[ListZipper] = ???

  // Implement the `Functor` instance for `MaybeListZipper`.
  //
  // >>> (+1) <$> (IsZ (zipper [3,2,1] 4 [5,6,7]))
  // [4,3,2] >5< [6,7,8]
  implicit val maybeListZipperFunctor: Functor[MaybeListZipper] = ???

  // Create a `MaybeListZipper` positioning the focus at the head.
  //
  // ->>> fromList (1 :. 2 :. 3 :. Nil)
  // [] >1< [2,3]
  //
  // >>> fromList Nil
  // ><
  //
  // prop> xs == toListZ (fromList xs)
  def fromList[A](list: List[A]): MaybeListZipper[A] = ???

  // Retrieve the `ListZipper` from the `MaybeListZipper` if there is one.
  //
  // prop> isEmpty xs == (toOptional (fromList xs) == Empty)
  //
  // prop> toOptional (fromOptional z) == z
  def toOptional[A](mz: MaybeListZipper[A]): Optional[ListZipper[A]] = ???

  def zipper[A](l: ScalaList[A], m: A, r: ScalaList[A]): ListZipper[A] =
    ListZipper(listScala(l), m, listScala(r))

  def fromOptional[A](oz: Optional[ListZipper[A]]): MaybeListZipper[A] =
    oz match {
      case Empty   => IsNotZ
      case Full(z) => IsZ(z)
    }

  def asZipper[A](mz: MaybeListZipper[A])(f: ListZipper[A] => ListZipper[A]): MaybeListZipper[A] =
    mz match {
      case IsNotZ => IsNotZ
      case IsZ(z) => IsZ(f(z))
    }

  def asMaybeZipper[A](mz: MaybeListZipper[A])(f: ListZipper[A] => MaybeListZipper[A]): MaybeListZipper[A] =
    mz match {
      case IsNotZ => IsNotZ
      case IsZ(z) => f(z)
    }

  // Convert the given zipper back to a list.
  //
  // >>> toList <$> toOptional (fromList Nil)
  // Empty
  //
  // >>> toList (ListZipper Nil 1 (2:.3:.4:.Nil))
  // [1,2,3,4]
  //
  // >>> toList (ListZipper (3:.2:.1:.Nil) 4 (5:.6:.7:.Nil))
  // [1,2,3,4,5,6,7]
  def toList[A](z: ListZipper[A]): List[A] = ???

  // Convert the given (maybe) zipper back to a list.
  def toListZ[A](mz: MaybeListZipper[A]): List[A] =
    mz match {
      case IsNotZ => Nil
      case IsZ(z) => toList(z)
    }

  // Update the focus of the zipper with the given function on the current focus.
  //
  // >>> withFocus (+1) (zipper [] 0 [1])
  // [] >1< [1]
  //
  // >>> withFocus (+1) (zipper [1,0] 2 [3,4])
  // [1,0] >3< [3,4]
  def withFocus[A](z: ListZipper[A])(f: A => A): ListZipper[A] = ???

  // Set the focus of the zipper to the given value.
  // /Tip:/ Use `withFocus`.
  //
  // >>> setFocus 1 (zipper [] 0 [1])
  // [] >1< [1]
  //
  // >>> setFocus 1 (zipper [1,0] 2 [3,4])
  // [1,0] >1< [3,4]
  def setFocus[A](z: ListZipper[A], a: A): ListZipper[A] = ???

  // Returns whether there are values to the left of focus.
  //
  // >>> hasLeft (zipper [1,0] 2 [3,4])
  // True
  //
  // >>> hasLeft (zipper [] 0 [1,2])
  // False
  def hasLeft[A](z: ListZipper[A]): Boolean = ???

  // Returns whether there are values to the right of focus.
  //
  // >>> hasRight (zipper [1,0] 2 [3,4])
  // True
  //
  // >>> hasRight (zipper [1,0] 2 [])
  // False
  def hasRight[A](z: ListZipper[A]): Boolean = ???

  // Seek to the left for a location matching a predicate, starting from the
  // current one.
  //
  // /Tip:/ Use `break`
  //
  // prop> findLeft (const p) -<< fromList xs == IsNotZ
  //
  // >>> findLeft (== 1) (zipper [2, 1] 3 [4, 5])
  // [] >1< [2,3,4,5]
  //
  // >>> findLeft (== 6) (zipper [2, 1] 3 [4, 5])
  // ><
  //
  // >>> findLeft (== 1) (zipper [2, 1] 1 [4, 5])
  // [] >1< [2,1,4,5]
  //
  // >>> findLeft (== 1) (zipper [1, 2, 1] 3 [4, 5])
  // [2,1] >1< [3,4,5]
  def findLeft[A](z: ListZipper[A])(p: A => Boolean): MaybeListZipper[A] = ???

  // Seek to the right for a location matching a predicate, starting from the
  // current one.
  //
  // /Tip:/ Use `break`
  //
  // prop> findRight (const False) -<< fromList xs == IsNotZ
  //
  // >>> findRight (== 5) (zipper [2, 1] 3 [4, 5])
  // [4,3,2,1] >5< []
  //
  // >>> findRight (== 6) (zipper [2, 1] 3 [4, 5])
  // ><
  //
  // >>> findRight (== 1) (zipper [2, 3] 1 [4, 5, 1])
  // [5,4,1,2,3] >1< []
  //
  // >>> findRight (== 1) (zipper [2, 3] 1 [1, 4, 5, 1])
  // [1,2,3] >1< [4,5,1]
  def findRight[A](z: ListZipper[A])(p: A => Boolean): MaybeListZipper[A] = ???

  // Move the zipper left, or if there are no elements to the left, go to the far right.
  //
  // >>> moveLeftLoop (zipper [3,2,1] 4 [5,6,7])
  // [2,1] >3< [4,5,6,7]
  //
  // >>> moveLeftLoop (zipper [] 1 [2,3,4])
  // [3,2,1] >4< []
  def moveLeftLoop[A](z: ListZipper[A]): ListZipper[A] = ???

  // Move the zipper right, or if there are no elements to the right, go to the far left.
  //
  // >>> moveRightLoop (zipper [3,2,1] 4 [5,6,7])
  // [4,3,2,1] >5< [6,7]
  //
  // >>> moveRightLoop (zipper [3,2,1] 4 [])
  // [] >1< [2,3,4]
  def moveRightLoop[A](z: ListZipper[A]): ListZipper[A] = ???

  // Move the zipper one position to the left.
  //
  // >>> moveLeft (zipper [3,2,1] 4 [5,6,7])
  // [2,1] >3< [4,5,6,7]
  //
  // >>> moveLeft (zipper [] 1 [2,3,4])
  // ><
  def moveLeft[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Move the zipper one position to the right.
  //
  // >>> moveRight (zipper [3,2,1] 4 [5,6,7])
  // [4,3,2,1] >5< [6,7]
  //
  // >>> moveRight (zipper [3,2,1] 4 [])
  // ><
  def moveRight[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Swap the current focus with the value to the left of focus.
  //
  // >>> swapLeft (zipper [3,2,1] 4 [5,6,7])
  // [4,2,1] >3< [5,6,7]
  //
  // >>> swapLeft (zipper [] 1 [2,3,4])
  // ><
  def swapLeft[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Swap the current focus with the value to the right of focus.
  //
  // >>> swapRight (zipper [3,2,1] 4 [5,6,7])
  // [3,2,1] >5< [4,6,7]
  //
  // >>> swapRight (zipper [3,2,1] 4 [])
  // ><
  def swapRight[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Drop all values to the left of the focus.
  //
  // >>> dropLefts (zipper [3,2,1] 4 [5,6,7])
  // [] >4< [5,6,7]
  //
  // >>> dropLefts (zipper [] 1 [2,3,4])
  // [] >1< [2,3,4]
  //
  // prop> dropLefts (zipper l x r) == zipper [] x r
  def dropLefts[A](z: ListZipper[A]): ListZipper[A] = ???

  // Drop all values to the right of the focus.
  //
  // >>> dropRights (zipper [3,2,1] 4 [5,6,7])
  // [3,2,1] >4< []
  //
  // >>> dropRights (zipper [3,2,1] 4 [])
  // [3,2,1] >4< []
  //
  // prop> dropRights (zipper l x r) == zipper l x []
  def dropRights[A](z: ListZipper[A]): ListZipper[A] = ???

  // Move the focus left the given number of positions. If the value is negative, move right instead.
  //
  // >>> moveLeftN 2 (zipper [2,1,0] 3 [4,5,6])
  // [0] >1< [2,3,4,5,6]
  //
  // >>> moveLeftN (-1) $ zipper [2,1,0] 3 [4,5,6]
  // [3,2,1,0] >4< [5,6]
  def moveLeftN[A](n: Int, z: ListZipper[A]): MaybeListZipper[A] = ???

  // Move the focus right the given number of positions. If the value is negative, move left instead.
  //
  // >>> moveRightN 1 (zipper [2,1,0] 3 [4,5,6])
  // [3,2,1,0] >4< [5,6]
  //
  // >>> moveRightN (-1) $ zipper [2,1,0] 3 [4,5,6]
  // [1,0] >2< [3,4,5,6]
  def moveRightN[A](n: Int, z: ListZipper[A]): MaybeListZipper[A] = ???

  // Move the focus left the given number of positions. If the value is negative, move right instead.
  // If the focus cannot be moved, the given number of times, return the value by which it can be moved instead.
  //
  // >>> moveLeftN' 4 (zipper [3,2,1] 4 [5,6,7])
  // Left 3
  //
  // >>> moveLeftN' 1 (zipper [3,2,1] 4 [5,6,7])
  // Right [2,1] >3< [4,5,6,7]
  //
  // >>> moveLeftN' 0 (zipper [3,2,1] 4 [5,6,7])
  // Right [3,2,1] >4< [5,6,7]
  //
  // >>> moveLeftN' (-2) (zipper [3,2,1] 4 [5,6,7])
  // Right [5,4,3,2,1] >6< [7]
  //
  // >>> moveLeftN' (-4) (zipper [3,2,1] 4 [5,6,7])
  // Left 3
  //
  // >>> moveLeftN' 4 (zipper [3,2,1] 4 [5,6,7,8,9])
  // Left 3
  //
  // >>> moveLeftN' (-4) (zipper [5,4,3,2,1] 6 [7,8,9])
  // Left 3
  def moveLeftNPrim[A](n: Int, z: ListZipper[A]): Either[Int, ListZipper[A]] = ???

  // Move the focus right the given number of positions. If the value is negative, move left instead.
  // If the focus cannot be moved, the given number of times, return the value by which it can be moved instead.
  //
  // >>> moveRightN' 4 (zipper [3,2,1] 4 [5,6,7])
  // Left 3
  //
  // >>> moveRightN' 1 (zipper [3,2,1] 4 [5,6,7])
  // Right [4,3,2,1] >5< [6,7]
  //
  // >>> moveRightN' 0 (zipper [3,2,1] 4 [5,6,7])
  // Right [3,2,1] >4< [5,6,7]
  //
  // >>> moveRightN' (-2) (zipper [3,2,1] 4 [5,6,7])
  // Right [1] >2< [3,4,5,6,7]
  //
  // >>> moveRightN' (-4) (zipper [3,2,1] 4 [5,6,7])
  // Left 3
  def moveRightNPrim[A](n: Int, z: ListZipper[A]): Either[Int, ListZipper[A]] = ???

  // Move the focus to the given absolute position in the zipper. Traverse the zipper only to the extent required.
  //
  // >>> nth 1 (zipper [3,2,1] 4 [5,6,7])
  // [1] >2< [3,4,5,6,7]
  //
  // >>> nth 5 (zipper [3,2,1] 4 [5,6,7])
  // [5,4,3,2,1] >6< [7]
  //
  // >>> nth 8 (zipper [3,2,1] 4 [5,6,7])
  // ><
  def nth[A](n: Int, z: ListZipper[A]): MaybeListZipper[A] = ???

  // Return the absolute position of the current focus in the zipper.
  //
  // >>> index (zipper [3,2,1] 4 [5,6,7])
  // 3
  //
  // prop> optional True (\z' -> index z' == i) (toOptional (nth i z))
  def index[A](z: ListZipper[A]): Int = ???

  // Move the focus to the end of the zipper.
  //
  // >>> end (zipper [3,2,1] 4 [5,6,7])
  // [6,5,4,3,2,1] >7< []
  //
  // prop> toList lz == toList (end lz)
  //
  // prop> rights (end lz) == Nil
  def end[A](z: ListZipper[A]): ListZipper[A] = ???

  // Move the focus to the start of the zipper.
  //
  // >>> start (zipper [3,2,1] 4 [5,6,7])
  // [] >1< [2,3,4,5,6,7]
  //
  // prop> toList lz == toList (start lz)
  //
  // prop> lefts (start lz) == Nil
  def start[A](z: ListZipper[A]): ListZipper[A] = ???

  // Delete the current focus and pull the left values to take the empty position.
  //
  // >>> deletePullLeft (zipper [3,2,1] 4 [5,6,7])
  // [2,1] >3< [5,6,7]
  //
  // >>> deletePullLeft (zipper [] 1 [2,3,4])
  // ><
  def deletePullLeft[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Delete the current focus and pull the right values to take the empty position.
  //
  // >>> deletePullRight (zipper [3,2,1] 4 [5,6,7])
  // [3,2,1] >5< [6,7]
  //
  // >>> deletePullRight (zipper [3,2,1] 4 [])
  // ><
  def deletePullRight[A](z: ListZipper[A]): MaybeListZipper[A] = ???

  // Insert at the current focus and push the left values to make way for the new position.
  //
  // >>> insertPushLeft 15 (zipper [3,2,1] 4 [5,6,7])
  // [4,3,2,1] >15< [5,6,7]
  //
  // >>> insertPushLeft 15 (zipper [] 1 [2,3,4])
  // [1] >15< [2,3,4]
  //
  // prop> optional False (==z) (toOptional (deletePullLeft (insertPushLeft i z)))
  def insertPushLeft[A](a: A, z: ListZipper[A]): ListZipper[A] = ???

  // Insert at the current focus and push the right values to make way for the new position.
  //
  // >>> insertPushRight 15 (zipper [3,2,1] 4 [5,6,7])
  // [3,2,1] >15< [4,5,6,7]
  //
  // >>> insertPushRight 15 (zipper [3,2,1] 4 [])
  // [3,2,1] >15< [4]
  //
  // prop> optional False (==z) (toOptional (deletePullRight (insertPushRight i z)))
  def insertPushRight[A](a: A, z: ListZipper[A]): ListZipper[A] = ???

  // Implement the `Applicative` instance for `ListZipper`.
  // `pure` produces an infinite list zipper (to both left and right).
  // (<*>) zips functions with values by function application.
  //
  // prop> all . (==) <*> take n . lefts . pure
  //
  // prop> all . (==) <*> take n . rights . pure
  //
  // >>> zipper [(+2), (+10)] (*2) [(*3), (4*), (5+)] <*> zipper [3,2,1] 4 [5,6,7]
  // [5,12] >8< [15,24,12]
  implicit val listZipperApplicative: Applicative[ListZipper] = new Applicative[ListZipper] {
    // /Tip:/ Use @List#repeat@.
    def pure[A](a: A): ListZipper[A] = ???
    // /Tip:/ Use `zipWith`
    def apply[A, B](fz: ListZipper[A => B])(z: ListZipper[A]): ListZipper[B] = ???
  }

  // Implement the `Applicative` instance for `MaybeListZipper`.
  //
  // /Tip:/ Use @pure@ for `ListZipper`.
  // /Tip:/ Use `<*>` for `ListZipper`.
  //
  // prop> let is (IsZ z) = z in all . (==) <*> take n . lefts . is . pure
  //
  // prop> let is (IsZ z) = z in all . (==) <*> take n . rights . is . pure
  //
  // >>> IsZ (zipper [(+2), (+10)] (*2) [(*3), (4*), (5+)]) <*> IsZ (zipper [3,2,1] 4 [5,6,7])
  // [5,12] >8< [15,24,12]
  //
  // >>> IsNotZ <*> IsZ (zipper [3,2,1] 4 [5,6,7])
  // ><
  //
  // >>> IsZ (zipper [(+2), (+10)] (*2) [(*3), (4*), (5+)]) <*> IsNotZ
  // ><
  //
  // >>> IsNotZ <*> IsNotZ
  // ><
  implicit val maybeListZipperApplicative: Applicative[MaybeListZipper] = ???

  // Implement the `Extend` instance for `ListZipper`.
  // This implementation "visits" every possible zipper value derivable from a given zipper (i.e. all zippers to the left and right).
  //
  // /Tip:/ Use @List#unfoldr@.
  //
  // >>> id <<= (zipper [2,1] 3 [4,5])
  // [[1] >2< [3,4,5],[] >1< [2,3,4,5]] >[2,1] >3< [4,5]< [[3,2,1] >4< [5],[4,3,2,1] >5< []]
  implicit val listZipperExtend: Extend[ListZipper] = ???

  // Implement the `Extend` instance for `MaybeListZipper`.
  // This instance will use the `Extend` instance for `ListZipper`.
  //
  //
  // id <<= IsNotZ
  // ><
  //
  // >>> id <<= (IsZ (zipper [2,1] 3 [4,5]))
  // [[1] >2< [3,4,5],[] >1< [2,3,4,5]] >[2,1] >3< [4,5]< [[3,2,1] >4< [5],[4,3,2,1] >5< []]
  implicit val maybeListZipperExtend: Extend[MaybeListZipper] = ???

  // Implement the `Comonad` instance for `ListZipper`.
  // This implementation returns the current focus of the zipper.
  //
  // >>> copure (zipper [2,1] 3 [4,5])
  // 3
  implicit val listZipperComonad: Comonad[ListZipper] = ???

  // Implement the `Traversable` instance for `ListZipper`.
  // This implementation traverses a zipper while running some `Applicative` effect through the zipper.
  // An effectful zipper is returned.
  //
  // >>> traverse id (zipper [Full 1, Full 2, Full 3] (Full 4) [Full 5, Full 6, Full 7])
  // Full [1,2,3] >4< [5,6,7]
  //
  // >>> traverse id (zipper [Full 1, Full 2, Full 3] (Full 4) [Empty, Full 6, Full 7])
  // Empty
  implicit val listZipperTraversable: Traversable[ListZipper] = ???

  // Implement the `Traversable` instance for `MaybeListZipper`.
  //
  // /Tip:/ Use `traverse` for `ListZipper`.
  //
  // >>> traverse id IsNotZ
  // ><
  //
  // >>> traverse id (IsZ (zipper [Full 1, Full 2, Full 3] (Full 4) [Full 5, Full 6, Full 7]))
  // Full [1,2,3] >4< [5,6,7]
  implicit val maybeListZipperTraversable: Traversable[MaybeListZipper] = ???







  import org.scalactic.TripleEquals._

  implicit def listZipperEq[A: Equality]: Equality[ListZipper[A]] = new Equality[ListZipper[A]] {

    def areEqual(a: ListZipper[A], other: Any): Boolean = other match {
      case b: ListZipper[_] =>
        a.l === b.l && a.m === b.m && a.r === b.r

      case _ => false
    }
  }

  implicit def maybeListZipperEq[A: Equality]: Equality[MaybeListZipper[A]] = new Equality[MaybeListZipper[A]] {

    def areEqual(a: MaybeListZipper[A], other: Any): Boolean = other match {
      case b: MaybeListZipper[_] =>
        (a, b) match {
          case (IsNotZ, IsNotZ)   => true
          case (IsZ(aa), IsZ(bb)) => aa === bb
          case _                  => false
        }

      case _ => false
    }
  }
}
